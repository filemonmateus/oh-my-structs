#include <iostream>
#include "lib/Cache.h"

// templated class shortener/alias
using LRUCache = Cache::LRUCache<int,int>;

int main() {
  LRUCache* cache = new LRUCache(2);
  cache->put(1,1);
  cache->put(2,2);
  std::cout << cache->get(1) << std::endl;
  cache->put(3,3);
  std::cout << cache->get(2) << std::endl;
  cache->put(4,4);
  std::cout << cache->get(1) << std::endl;
  std::cout << cache->get(3) << std::endl;
  std::cout << cache->get(4) << std::endl;
  std::cout << cache << std::endl;
  delete cache;
}

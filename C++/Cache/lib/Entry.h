#ifndef ENTRY_H_INCLUDED
#define ENTRY_H_INCLUDED

#include <ostream>

namespace Cache {
  template<typename K, typename V>
  class Entry {
    public:
      K key;
      V val;
      Entry(K key, V val);
      template<typename __K, typename __V> friend std::ostream& operator << (std::ostream& out, const Entry<__K, __V>& entry);
      template<typename __K, typename __V> friend std::ostream& operator << (std::ostream& out, const Entry<__K, __V>* entry);
  };

  template<typename K, typename V>
  Entry<K, V>::Entry(K key, V val) {
    this->key = key;
    this->val = val;
  }

  template<typename K, typename V>
  std::ostream& operator << (std::ostream& out, const Entry<K,V>& entry) {
    out << "{" << entry.key << ":" << entry.val << "}"; return out;
  }

  template<typename K, typename V>
  std::ostream& operator << (std::ostream& out, const Entry<K,V>* entry) {
    out << "{" << entry->key << ":" << entry->val << "}"; return out;
  }
}

#endif

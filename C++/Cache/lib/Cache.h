#ifndef CACHE_H_INCLUDED
#define CACHE_H_INCLUDED

#include <list>
#include <cassert>
#include <ostream>
#include <unordered_map>

#include "Entry.h"

namespace Cache {
  template<typename K, typename V>
  class LRUCache {
    private:
      uint msize;
      uint capacity;
      std::unordered_map<K, Entry<K,V>*> entries;
      std::list<Entry<K,V>*> elements;

    public:
      LRUCache(const uint capacity=10);
      virtual ~LRUCache();
      V get(const K& key);
      void put(const K& key, const V& val);
      bool exist(const K& key) const;
      uint size() const;
      template<typename __K, typename __V> friend std::ostream& operator << (std::ostream& out, const LRUCache<__K, __V>& cache);
      template<typename __K, typename __V> friend std::ostream& operator << (std::ostream& out, const LRUCache<__K, __V>* cache);
  };

  template<typename K, typename V>
  LRUCache<K,V>::LRUCache(const uint capacity) {
    assert(capacity > 0 && ":< oops, capacity must be a strictly positive int!");
    this->msize = 0;
    this->capacity = capacity;
  }

  template<typename K, typename V>
  LRUCache<K,V>::~LRUCache() {
    for (Entry<K,V>* element : this->elements) {
      delete element;
    }
  }

  template<typename K, typename V>
  V LRUCache<K,V>::get(const K& key) {
    if (!this->exist(key)) return (V) -1;
    Entry<K,V>* entry = this->entries[key];
    this->elements.remove(entry);
    this->elements.push_back(entry);
    return entry->val;
  }

  template<typename K, typename V>
  void LRUCache<K,V>::put(const K& key, const V& val) {
    if (this->exist(key)) this->elements.remove(this->entries[key]);
    Entry<K,V>* entry = new Entry<K,V>(key, val);
    this->elements.push_back(entry);
    this->entries[key] = entry;

    if (this->entries.size() > this->capacity) {
      entry = this->elements.front();
      this->elements.remove(entry);
      this->entries.erase(entry->key);
      delete entry;
    }
  }

  template<typename K, typename V>
  bool LRUCache<K,V>::exist(const K& key) const {
    return this->entries.count(key) != 0;
  }

  template<typename K, typename V>
  uint LRUCache<K,V>::size() const {
    return this->msize;
  }

  template<typename K, typename V>
  std::ostream& operator << (std::ostream& out, const LRUCache<K,V>& cache) {
    for (Entry<K,V>* entry : cache.elements)
      out << entry << "->";
    out << "null";
    return out;
  }

  template<typename K, typename V>
  std::ostream& operator << (std::ostream& out, const LRUCache<K,V>* cache) {
    for (Entry<K,V>* entry : cache->elements)
      out << entry << "->";
    out << "null";
    return out;
  }
};

#endif

#ifndef HEAP_H_INCLUDED
#define HEAP_H_INCLUDED

#include "../../Vector/lib/Vector.h"

namespace Heap {
  template<typename T, typename Comparator=std::less<T>>
  class Heap : public Vector::Vector<T> {
    private:
      Comparator compare;
      void build_heap();
      void heapify(const int idx);

    public:
      Heap(const uint capacity=10);
      void push(const T element);
      void pop();
      T top();
  };

  template<typename T, typename Comparator>
  Heap<T, Comparator>::Heap(const uint capacity) : Vector::Vector<T>(capacity) {
    this->compare = Comparator();
  }

  template<typename T, typename Comparator>
  void Heap<T, Comparator>::build_heap() {
    for (int i = this->size() / 2; i >= 0; --i) {
      this->heapify(i);
    }
  }

  template<typename T, typename Comparator>
  void Heap<T, Comparator>::heapify(const int idx) {
    int curr = idx, left = 2*idx, right = 2*idx+1;
    if (left < this->size() && this->compare(this->elements[curr], this->elements[left])) curr = left;
    if (right < this->size() && this->compare(this->elements[curr], this->elements[right])) curr = right;
    if (curr != idx) { std::swap(this->elements[idx], this->elements[curr]); this->heapify(curr); }
  }

  template<typename T, typename Comparator>
  void Heap<T, Comparator>::push(const T element) {
    Vector::Vector<T>::push(element);
    if (this->compare(this->top(), element)) this->build_heap();
  }

  template<typename T, typename Comparator>
  void Heap<T, Comparator>::pop() {
    Vector::Vector<T>::remove(0);
    this->build_heap();
  }

  template<typename T, typename Comparator>
  T Heap<T, Comparator>::top() {
    return Vector::Vector<T>::get(0);
  }
}

#endif

#include <iostream>
#include "lib/Heap.h"

// templated class shortener/alias
using MaxHeap = Heap::Heap<int>; // defaults to maxheap (i.e. std::less<int> is passed internally as a generic typename for the comparator operator)
using MinHeap = Heap::Heap<int, std::greater<int>>;

int main() {
  MaxHeap* maxheap = new MaxHeap;
  maxheap->push(4);
  maxheap->push(3);
  maxheap->push(7);
  maxheap->push(9);
  maxheap->push(1);
  std::cout << maxheap << std::endl;

  MinHeap* minheap = new MinHeap;
  minheap->push(4);
  minheap->push(3);
  minheap->push(7);
  minheap->push(9);
  minheap->push(1);
  std::cout << minheap << std::endl;

  delete maxheap;
  delete minheap;
}

/*
 * Oh-my-trie: a minimally viable prefix trie with autocomplete
 */

#include "lib/Trie.h"

// templated class shortener/alias
using PrefixTrie = Trie::PrefixTrie;

int main() {
  // load dict.txt (3.69Mb) into memory
  PrefixTrie* trie = new PrefixTrie("dict.txt");

  // exhaustively search all matches for query "file"
  trie->autocomplete("file");

  // unload dict.txt (optional!)
  trie->unload();

  // that's all
  delete trie;
}

#ifndef TRIE_H_INCLUDED
#define TRIE_H_INCLUDED

#include "Node.h"
#include <string>
#include <fstream>

namespace Trie {
  class PrefixTrie {
    private:
      Node* root;
      uint words;
      void _autocomplete(std::string prefix, Node* current) const;
      void _unload(Node* current);

    public:
      PrefixTrie();
      PrefixTrie(const std::string dictionary);
      virtual ~PrefixTrie();
      void add(const std::string word);
      bool check(const std::string word) const;
      void load(const std::string dictionary);
      bool empty() const;
      uint size() const;
      void autocomplete(std::string prefix) const;
      void unload();
      friend std::ostream& operator << (std::ostream& out, const PrefixTrie& trie);
      friend std::ostream& operator << (std::ostream& out, const PrefixTrie* trie);
  };
}

#endif

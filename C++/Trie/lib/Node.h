#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED
#define ALPHABET 26

#include <iostream>

namespace Trie {
  class Node {
    public:
      bool word;
      Node* children[ALPHABET];
      Node(void);
  };
}

#endif

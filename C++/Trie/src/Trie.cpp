#include "../lib/Trie.h"

namespace Trie {
  PrefixTrie::PrefixTrie(void) {
    this->words = 0;
    this->root = new Node;
  }

  PrefixTrie::PrefixTrie(const std::string dictionary) {
    this->words = 0;
    this->root = new Node;
    this->load(dictionary);
  }

  PrefixTrie::~PrefixTrie(void) {
    this->_unload(this->root);
  }

  void PrefixTrie::add(const std::string word) {
    Node* curr = this->root;
    for (int i = 0; i < word.length(); ++i) {
      int idx = std::tolower(word[i]) - 'a';
      if (curr->children[idx] == nullptr) curr->children[idx] = new Node;
      curr = curr->children[idx];
    }
    curr->word = true;
    this->words++;
  }

  bool PrefixTrie::check(const std::string word) const {
    Node* curr = this->root;
    for (int i = 0; i < word.length(); ++i) {
      int idx = std::tolower(word[i]) - 'a';
      if (curr->children[idx] == nullptr) return false;
      curr = curr->children[idx];
    }
    return curr->word;
  }

  void PrefixTrie::load(const std::string dictionary) {
    std::ifstream dict(dictionary.c_str());
    if (dict.fail()) throw std::ios_base::failure(":< oops, fail to open input file stream!");
    std::string word;
    while (getline(dict, word)) this->add(word); 
    dict.close();
  }

  bool PrefixTrie::empty(void) const {
    return this->words == 0;
  }

  uint PrefixTrie::size(void) const {
    return this->words;
  }

  void PrefixTrie::_autocomplete(std::string prefix, Node* curr) const {
    if (curr->word) std::cout << prefix << std::endl;
    for (int i = 0; i < ALPHABET; ++i) {
      if (curr->children[i]) {
        prefix.push_back(i+'a');
        this->_autocomplete(prefix, curr->children[i]);
        prefix.pop_back();
      }
    }
  }

  void PrefixTrie::autocomplete(std::string prefix) const {
    Node* curr = this->root;
    for (int i = 0; i < prefix.length(); ++i) {
      int idx = std::tolower(prefix[i]) - 'a';
      curr = curr->children[idx];
    }
    this->_autocomplete(prefix, curr);
  }

  void PrefixTrie::_unload(Node* curr) {
    if (curr == nullptr) return;
    for (int i = 0; i < ALPHABET; ++i) {
      if (curr->children[i]) {
        this->_unload(curr->children[i]);
      }
    }
    delete curr;
  }

  void PrefixTrie::unload(void) {
    this->_unload(this->root);
    this->words = 0;
    this->root = new Node;
  }

  std::ostream& operator << (std::ostream& out, const PrefixTrie& trie) {
    trie._autocomplete("", trie.root);
    return out;
  }

  std::ostream& operator << (std::ostream& out, const PrefixTrie* trie) {
    trie->_autocomplete("", trie->root);
    return out;
  }
}

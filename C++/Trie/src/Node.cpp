#include "../lib/Node.h"

namespace Trie {
  Node::Node(void) {
    for (int i = 0; i < ALPHABET; ++i) {
      this->children[i] = nullptr;
    }
    this->word = false;
  }
}

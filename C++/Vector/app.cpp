#include <iostream>
#include "lib/Vector.h"

// templated class shortener/alias
using BasicVector = Vector::Vector<int>;

int main() {
  // init vector
  BasicVector* v = new BasicVector();

  // append ints
  v->push(2);
  v->push(3);
  v->push(5);
  v->push(7);

  // insert ints
  v->insert(0,1);
  v->insert(3,4);
  v->insert(5,6);

  // inspect vector
  std::cout << v << std::endl;

  // that's all
  delete v;
}

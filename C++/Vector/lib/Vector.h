#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include <cassert>
#include <ostream>
#include <iterator>
#include <functional>

namespace Vector {
  template<typename T>
  class Vector {
    protected:
      uint msize;
      uint capacity;
      T* elements;

    public:
      Vector(const uint capacity=10);
      virtual ~Vector();
      void push(const T element);
      void insert(const int index, const T element);
      void remove(const int index);
      void erase(const T element);
      void reverse();
      void resize();
      void set(const int index, const T element);
      T& get(const int index) const;
      bool lookup(const T element) const;
      bool full() const;
      bool empty() const;
      uint size() const;
      T* begin() const;
      T* end() const;
      const T& operator[] (const int index) const;
      T& operator[] (const int index);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Vector<__T>& vec);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Vector<__T>* vec);
  };

  template<typename T>
  Vector<T>::Vector(const uint capacity) {
    assert(capacity > 0 && ":< oops, capacity must be a stricly positive int!");
    this->msize = 0;
    this->capacity = capacity;
    this->elements = new T[this->capacity];
  }

  template<typename T>
  Vector<T>::~Vector() {
    delete[] this->elements;
  }

  template<typename T>
  void Vector<T>::push(const T element) {
    if (this->msize == this->capacity) this->resize();
    this->elements[this->msize++] = element;
  }

  template<typename T>
  void Vector<T>::insert(const int index, const T element) {
    assert(index < this->capacity);
    if (this->msize == this->capacity) this->resize();
    for (int i = this->msize; i > index; --i) {
      this->elements[i] = this->elements[i-1];
    }
    this->elements[index] = element;
    this->msize++;
  }

  template<typename T>
  void Vector<T>::remove(const int index) {
    assert(index < this->msize);
    for (int i = index; i < this->msize - 1; ++i) {
      this->elements[i] = this->elements[i+1];
    }
    this->msize--;
  }

  template<typename T>
  void Vector<T>::erase(const T element) {
    for (int i = 0; i < this->msize; ++i) {
      if (this->elements[i] == element) {
        this->remove(i);
      }
    }
  }

  template<typename T>
  void Vector<T>::reverse() {
    for (int i = 0; i < this->msize/2; ++i) {
      std::swap(this->elements[i], this->elements[this->msize-i-1]);
    }  
  }

  template<typename T>
  void Vector<T>::resize() {
    this->capacity *= 2;
    T* elements = new T[this->capacity];
    for (int i = 0; i < this->msize; ++i) {
      elements[i] = this->elements[i];
    }
    delete[] this->elements;
    this->elements = elements;
  }

  template<typename T>
  void Vector<T>::set(const int index, const T element) {
    assert(index < this->capacity);
    this->elements[index] = element;
  }

  template<typename T>
  T& Vector<T>::get(const int index) const {
    assert(index < this->msize);
    return this->elements[index];
  }

  template<typename T>
  bool Vector<T>::full() const {
    return this->msize == this->capacity;
  }

  template<typename T>
  bool Vector<T>::empty() const {
    return this->msize == 0;
  }

  template<typename T>
  uint Vector<T>::size() const {
    return this->msize;
  }

  template<typename T>
  T* Vector<T>::begin() const {
    return &this->elements[0];
  }

  template<typename T>
  T* Vector<T>::end() const {
    return &this->elements[this->msize];
  }

  template<typename T>
  const T& Vector<T>::operator[] (const int index) const {
    return this->elements[index];
  }

  template<typename T>
  T& Vector<T>::operator[] (const int index) {
    return this->elements[index];
  }
  
  template<typename T>
  std::ostream& operator << (std::ostream& out, const Vector<T>& vec) {
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(out, " ")); return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Vector<T>* vec) {
    std::copy(vec->begin(), vec->end(), std::ostream_iterator<T>(out, " ")); return out;
  }
}

#endif

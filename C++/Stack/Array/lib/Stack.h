#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

#include <cassert>
#include <ostream>

namespace Stack {
  template<typename T>
  class StackArray {
    private:
      uint msize;
      uint capacity;
      T* elements;

    public:
      StackArray(const uint capacity=10);
      virtual ~StackArray();
      void push(const T element);
      void pop();
      T top() const;
      void resize();
      bool full() const;
      bool empty() const;
      uint size() const;
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const StackArray<__T>& s);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const StackArray<__T>* s);
  };

  template<typename T>
  StackArray<T>::StackArray(const uint capacity) {
    assert(capacity > 0 && ":< oops, capacity must be a strictly positive int!");
    this->msize = 0;
    this->capacity = capacity;
    this->elements = new T[this->capacity];
  }

  template<typename T>
  StackArray<T>::~StackArray() {
    delete[] this->elements;
  }

  template<typename T>
  void StackArray<T>::push(const T element) {
    if (this->full()) this->resize();
    this->elements[this->msize++] = element;
  }

  template<typename T>
  void StackArray<T>::pop() {
    if (this->empty()) throw std::length_error(":< oops, can\'t call pop on empty stack!");
    this->msize--;
  }
  
  template<typename T>
  T StackArray<T>::top() const {
    if (this->empty()) throw std::length_error(":< oops, can\'t call top on empty stack!");
    return this->elements[this->msize-1];
  }

  template<typename T>
  void StackArray<T>::resize() {
    this->capacity *= 2;
    T* elements = new T[this->capacity];
    for (int i = 0; i < this->msize; ++i)  {
      elements[i] = this->elements[i];
    }
    delete[] this->elements;
    this->elements = elements;
  }

  template<typename T>
  bool StackArray<T>::full() const {
    return this->msize == this->capacity;
  }

  template<typename T>
  bool StackArray<T>::empty() const {
    return this->msize == 0;
  }

  template<typename T>
  uint StackArray<T>::size() const {
    return this->msize;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const StackArray<T>& s) {
    for (int i = s.msize - 1; i >= 0; --i)
      out << s.elements[i] << " ";
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const StackArray<T>* s) {
    for (int i = s->msize - 1; i >= 0; --i)
      out << s->elements[i] << " ";
    return out;
  }
}

#endif

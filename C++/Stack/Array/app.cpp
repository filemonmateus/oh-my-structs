#include <iostream>
#include "lib/Stack.h"

// templated class shortener/alias
using BasicStack = Stack::StackArray<int>;

int main() {
  // init stack
  BasicStack* s = new BasicStack;

  // push keys
  s->push(1);
  s->push(2);
  s->push(3);
  s->push(4);
  s->push(5);

  // inspect them
  std::cout << s << std::endl;

  // inspect LIFO invariance
  std::cout << "top() -> " << s->top() << std::endl;
  s->pop();
  std::cout << "top() -> " << s->top() << std::endl;
  s->pop();

  // inspect s state
  std::cout << s << std::endl;

  // that's all
  delete s;
}

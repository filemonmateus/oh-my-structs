#ifndef Stack_List_Included
#define Stack_List_Included

#include "../../../List/SLL/lib/List.h"

namespace Stack {
  template<typename T>
  class StackList : public List::SinglyLinkedList<T> {
    public:
      StackList();
      void push(const T key);
      void pop();
      T top() const;
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const StackList<__T>& s);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const StackList<__T>* s);
  };

  template<typename T>
  StackList<T>::StackList() : List::SinglyLinkedList<T>() {}

  template<typename T>
  void StackList<T>::push(const T key) {
    this->prepend(key);
  }

  template<typename T>
  void StackList<T>::pop() {
    if (this->empty()) throw std::length_error(":< oops, can\'t call pop on empty stack!");
    this->remove(this->head->key);
  }

  template<typename T>
  T StackList<T>::top() const {
    if (this->empty()) throw std::length_error(":< oops, can\'t call top on empty stack!");
    return this->head->key;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const StackList<T>& s) {
    for (List::Node<T>* curr = s.head; curr; curr = curr->next)
      out << curr << " ";
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const StackList<T>* s) {
    for (List::Node<T>* curr = s->head; curr; curr = curr->next)
      out << curr << " ";
    return out;
  }
}

#endif

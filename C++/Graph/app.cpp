#include <iostream>
#include "lib/Graph.h"

// templated class shortener/alias 
using Vertex = Graph::Vertex<std::string>;
using Edge = Graph::Edge<std::string>;
using BasicGraph = Graph::Graph<std::string>;

int main() {
  // init graph
  BasicGraph* graph = new BasicGraph;

  // add airports
  graph->add_vertex("HNL");
  graph->add_vertex("SFO");
  graph->add_vertex("LAX");
  graph->add_vertex("ORD");
  graph->add_vertex("DFW");
  graph->add_vertex("PVD");
  graph->add_vertex("LGA");
  graph->add_vertex("MIA");

  // then flights and related routes
  graph->add_edge("HNL", "MIA", 500);
  graph->add_edge("HNL", "SFO", 130);
  graph->add_edge("SFO", "HNL", 130);
  graph->add_edge("SFO", "LAX", 60);
  graph->add_edge("SFO", "ORD", 70);
  graph->add_edge("LAX", "HNL", 250);
  graph->add_edge("LAX", "SFO", 60);
  graph->add_edge("LAX", "ORD", 170);
  graph->add_edge("LAX", "DFW", 120);
  graph->add_edge("ORD", "SFO", 70);
  graph->add_edge("ORD", "LAX", 170);
  graph->add_edge("ORD", "DFW", 80);
  graph->add_edge("ORD", "PVD", 50);
  graph->add_edge("DFW", "LAX", 120);
  graph->add_edge("DFW", "ORD", 80);
  graph->add_edge("DFW", "LGA", 140);
  graph->add_edge("DFW", "MIA", 110);
  graph->add_edge("PVD", "ORD", 50);
  graph->add_edge("PVD", "LGA", 200);
  graph->add_edge("LGA", "DFW", 140);
  graph->add_edge("LGA", "MIA", 100);
  graph->add_edge("MIA", "DFW", 110);
  graph->add_edge("MIA", "HNL", 500);

  // heuristic unweighted estimator
  auto dist_metric = [] (const Vertex* src, const Vertex* dst) { 
    return 0.0;
  };

  // inspect A* cheapest route from San Francisco to Miami
  std::cout << "A* cheapest route (SFO->MIA)" << std::endl;
  for (Vertex* vertex : graph->astar("SFO", "MIA", dist_metric)) {
    if (vertex->prev) {
      std::cout << vertex->prev << "->" << vertex << " $(" << vertex->cost - vertex->prev->cost << ")" << std::endl;
    }
  }
  std::cout << std::endl;

  // inspect Dijkstra cheapest route from San Francisco to Miami
  std::cout << "Dijkstra cheapest route (SFO->MIA)" << std::endl;
  for (Vertex* vertex : graph->dijkstra("SFO", "MIA")) {
    if (vertex->prev) {
      std::cout << vertex->prev << "->" << vertex << " $(" << vertex->cost - vertex->prev->cost << ")" << std::endl;
    }
  }
  std::cout << std::endl;

  // inspect bfs route from San Francisco to Miami
  std::cout << "Fastest BFS route (SFO->MIA)" << std::endl;
  for (Vertex* vertex : graph->bfs("SFO", "MIA")) {
    if (vertex->prev) {
      std::cout << vertex->prev << "->" << vertex << " $(" << vertex->cost - vertex->prev->cost << ")" << std::endl;
    }
  }
  std::cout << std::endl;

  // inspect dfs route from San Francisco to Miami
  std::cout << "Random DFS route (SFO->MIA)" << std::endl;
  for (Vertex* vertex : graph->dfs("SFO", "MIA")) {
    if (vertex->prev) {
      std::cout << vertex->prev << "->" << vertex << " $(" << vertex->cost - vertex->prev->cost << ")" << std::endl;
    }
  }

  // that's all
  delete graph;
}

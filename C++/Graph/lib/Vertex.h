#ifndef VERTEX_H_INCLUDED
#define VERTEX_H_INCLUDED

#include <limits>
#include <ostream>

#include "../../Vector/lib/Vector.h"

namespace Graph {
  template<typename T>
  class Vertex {
    public:
      T key;
      double cost;
      bool visited;
      Vertex<T>* prev;
      Vector::Vector<Vertex<T>*> neighbors;
      Vertex(const T key);
      virtual ~Vertex();
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Vertex<__T>& vertex);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Vertex<__T>* vertex);
  };

  template<typename T>
  Vertex<T>::Vertex(const T key) {
    this->key = key;
    this->cost = std::numeric_limits<double>::max();
    this->prev = nullptr;
    this->visited = false;
  }

  template<typename T>
  Vertex<T>::~Vertex() {
    this->prev = nullptr;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Vertex<T>& vertex) {
    out << vertex.key; return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Vertex<T>* vertex) {
    out << vertex->key; return out;
  }
}

#endif

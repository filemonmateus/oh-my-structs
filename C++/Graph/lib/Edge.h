#ifndef EDGE_H_INCLUDED
#define EDGE_H_INCLUDED

#include "Vertex.h"

namespace Graph {
  template<typename T>
  class Edge {
    public:
      double weight;
      Vertex<T>* src;
      Vertex<T>* dst;
      Edge(Vertex<T>* src, Vertex<T>* dst, const double weight=0.0);
      virtual ~Edge();
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Edge<__T>& edge);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Edge<__T>* edge);
  };

  template<typename T>
  Edge<T>::Edge(Vertex<T>* src, Vertex<T>* dst, const double weight) {
    this->src = src;
    this->dst = dst;
    this->weight = weight;
    this->src->neighbors.push(this->dst);
  }

  template<typename T>
  Edge<T>::~Edge() {
    this->src->neighbors.erase(this->dst);
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Edge<T>& edge) {
    out << edge.src << "->" << edge.dst << " (" << edge.weight << ")"; return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Edge<T>* edge) {
    out << edge->src << "->" << edge->dst << " (" << edge->weight << ")"; return out;
  }
}

#endif

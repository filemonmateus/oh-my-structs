#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED

#include <functional>

#include "Vertex.h"
#include "Edge.h"
#include "../../Vector/lib/Vector.h"
#include "../../Stack/Array/lib/Stack.h"
#include "../../Queue/Array/lib/Queue.h"
#include "../../pQueue/Heap/lib/pQueue.h"

namespace Graph {
  template<typename T>
  class Graph {
    public:
      virtual ~Graph();
      void add_vertex(const T vertex_key);
      void add_edge(const T src_key, const T dst_key, const double weight=0.0);
      void remove_vertex(const T vertex_key);
      void remove_edge(const T src_key, const T dst_key, const double weight=0.0);
      Vertex<T>* get_vertex(const T vertex_key);
      Edge<T>* get_edge(const Vertex<T>* src_vertex, const Vertex<T>* dst_vertex, const double weight=0.0);
      Vector::Vector<Vertex<T>*> dfs(const T src);
      Vector::Vector<Vertex<T>*> bfs(const T src);
      Vector::Vector<Vertex<T>*> dfs(const T src, const T dst);
      Vector::Vector<Vertex<T>*> bfs(const T src, const T dst);
      Vector::Vector<Vertex<T>*> dijkstra(const T src, const T dst);
      Vector::Vector<Vertex<T>*> astar(const T src, const T dst, const std::function<double (const Vertex<T>* src, const Vertex<T>* dst)>& heuristic);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Graph<__T>& graph);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Graph<__T>* graph);

    private:
      Vector::Vector<Vertex<T>*> vertices;
      Vector::Vector<Edge<T>*> edges;
      void mark_all_vertices_unvisited();
  };

  template<typename T>
  Graph<T>::~Graph() {
    for (Edge<T>* edge : this->edges) {
      delete edge;
    }
    for (Vertex<T>* vertex : this->vertices) {
      delete vertex;
    }
  }

  template<typename T>
  void Graph<T>::add_vertex(const T vertex_key) {
    if (!this->get_vertex(vertex_key)) this->vertices.push(new Vertex<T>(vertex_key));
  }

  template<typename T>
  void Graph<T>::add_edge(const T src_key, const T dst_key, const double weight) {
    // inspect src_vertex and dst_vertex
    Vertex<T>* src_vertex = this->get_vertex(src_key);
    Vertex<T>* dst_vertex = this->get_vertex(dst_key);

    // inspect edge, if there isn't one already, add one
    if (src_vertex && dst_vertex && !this->get_edge(src_vertex, dst_vertex, weight)) {
      this->edges.push(new Edge<T>(src_vertex, dst_vertex, weight));
    }
  }

  template<typename T>
  void Graph<T>::remove_vertex(const T vertex_key) {
    Vertex<T>* vertex = this->get_vertex(vertex_key);
    if (vertex) {
      // edges first
      for (auto it = this->edges.begin(); it != this->edges.end(); ++it) {
        Edge<T>* edge = *it;
        if (edge->src == vertex || edge->dst == vertex) {
          this->edges.erase(edge);
          delete edge;
          --it;
        }
      }
      // then vertex
      this->vertices.erase(vertex);
      delete vertices;
    }
  }

  template<typename T>
  void Graph<T>::remove_edge(const T src_key, const T dst_key, const double weight) {
    // inspect src and dst vertices
    Vertex<T>* src_vertex = this->get_vertex(src_key);
    Vertex<T>* dst_vertex = this->get_vertex(dst_key);
    Edge<T>* edge = this->get_edge(src_vertex, dst_vertex, weight);

    // delete edge, if there is one!
    if (src_vertex && dst_vertex && edge) {
      this->edges.erase(edge);
      delete edge;
    }
  }

  template<typename T>
  Vertex<T>* Graph<T>::get_vertex(const T vertex_key) {
    for (Vertex<T>* vertex : this->vertices) {
      if (vertex->key == vertex_key) {
        return vertex;
      }
    }
    return nullptr;
  }

  template<typename T>
  Edge<T>* Graph<T>::get_edge(const Vertex<T>* src_vertex, const Vertex<T>* dst_vertex, const double weight) {
    for (Edge<T>* edge : this->edges) {
      if (weight) {
        if (edge->src == src_vertex && edge->dst == dst_vertex && edge->weight == weight) {
          return edge;
        }
      }
      else {
        if (edge->src == src_vertex && edge->dst == dst_vertex) {
          return edge;
        }
      }
    }
    return nullptr;
  }

  template<typename T>
  Vector::Vector<Vertex<T>*> Graph<T>::dfs(const T src) {
    // house keeping
    this->mark_all_vertices_unvisited();
    Vector::Vector<Vertex<T>*> path;

    // inspect src vertex
    Vertex<T>* src_vertex = this->get_vertex(src);
    if (!src_vertex) return path;

    // apply depth-first search from src
    Stack::StackArray<Vertex<T>*>* visited = new Stack::StackArray<Vertex<T>*>();
    src_vertex->cost = 0.0;
    src_vertex->visited = true;
    visited->push(src_vertex);

    while (!visited->empty()) {
      Vertex<T>* vertex = visited->top();
      visited->pop();
      path.push(vertex);

      for (Vertex<T>* neighbor : vertex->neighbors) {
        if (!neighbor->visited) {
          neighbor->cost = vertex->cost + this->get_edge(vertex, neighbor)->weight;
          neighbor->prev = vertex;
          neighbor->visited = true;
          visited->push(neighbor);
        }
      }
    }

    delete visited;
    return path;
  }

  template<typename T>
  Vector::Vector<Vertex<T>*> Graph<T>::bfs(const T src) {
    // house keeping
    this->mark_all_vertices_unvisited();
    Vector::Vector<Vertex<T>*> path;

    // inspect src vertex
    Vertex<T>* src_vertex = this->get_vertex(src);
    if (!src_vertex) return path;

    // apply breadth-first search from src
    Queue::QueueArray<Vertex<T>*>* visited = new Queue::QueueArray<Vertex<T>*>();
    src_vertex->cost = 0.0;
    src_vertex->visited = true;
    visited->push(src_vertex);

    while (!visited->empty()) {
      Vertex<T>* vertex = visited->front();
      visited->pop();
      path.push(vertex);

      for (Vertex<T>* neighbor : vertex->neighbors) {
        if (!neighbor->visited) {
          neighbor->cost = vertex->cost + this->get_edge(vertex, neighbor)->weight;
          neighbor->prev = vertex;
          neighbor->visited = true;
          visited->push(neighbor);
        }
      }
    }

    delete visited;
    return path;
  }

  template<typename T>
  Vector::Vector<Vertex<T>*> Graph<T>::dfs(const T src, const T dst) {
    // house keeping
    this->mark_all_vertices_unvisited();
    Vector::Vector<Vertex<T>*> path;

    // inspect src and dst vertices
    Vertex<T>* src_vertex = this->get_vertex(src);
    Vertex<T>* dst_vertex = this->get_vertex(dst);
    if (!src_vertex || !dst_vertex) return path;

    // apply depth-first search from src and do early-stopping if dst is reached!
    Stack::StackArray<Vertex<T>*>* visited = new Stack::StackArray<Vertex<T>*>();
    src_vertex->cost = 0.0;
    src_vertex->visited = true;
    visited->push(src_vertex);

    while (!visited->empty()) {
      Vertex<T>* vertex = visited->top();
      visited->pop();
      if (vertex == dst_vertex) {
        // cool, now reconstruct dfs path from dst
        for (Vertex<T>* curr = dst_vertex; curr != src_vertex->prev; curr = curr->prev) {
          path.push(curr);
        }
        path.reverse();
        break;
      }

      for (Vertex<T>* neighbor : vertex->neighbors) {
        if (!neighbor->visited) {
          neighbor->cost = vertex->cost + this->get_edge(vertex, neighbor)->weight;
          neighbor->prev = vertex;
          neighbor->visited = true;
          visited->push(neighbor);
        }
      }
    }

    delete visited;
    return path;
  }

  template<typename T>
  Vector::Vector<Vertex<T>*> Graph<T>::bfs(const T src, const T dst) {
    // house keeping
    this->mark_all_vertices_unvisited();
    Vector::Vector<Vertex<T>*> path;

    // inspect src and dst vertices
    Vertex<T>* src_vertex = this->get_vertex(src);
    Vertex<T>* dst_vertex = this->get_vertex(dst);
    if (!src_vertex || !dst_vertex) return path;

    // apply breadth-first search from src and do early-stopping if dst is reached!
    Queue::QueueArray<Vertex<T>*>* visited = new Queue::QueueArray<Vertex<T>*>();
    src_vertex->cost = 0.0;
    src_vertex->visited = true;
    visited->push(src_vertex);

    while (!visited->empty()) {
      Vertex<T>* vertex = visited->front();
      visited->pop();
      if (vertex == dst_vertex) {
        // cool, now reconstruct bfs path from dst
        for (Vertex<T>* curr = dst_vertex; curr != src_vertex->prev; curr = curr->prev) {
          path.push(curr);
        }
        path.reverse();
        break;
      }

      for (Vertex<T>* neighbor : vertex->neighbors) {
        if (!neighbor->visited) {
          neighbor->cost = vertex->cost + this->get_edge(vertex, neighbor)->weight;
          neighbor->prev = vertex;
          neighbor->visited = true;
          visited->push(neighbor);
        }
      }
    }

    delete visited;
    return path;
  }

  template<typename T>
  Vector::Vector<Vertex<T>*> Graph<T>::dijkstra(const T src, const T dst) {
    // house keeping
    this->mark_all_vertices_unvisited();
    Vector::Vector<Vertex<T>*> path;

    // inspect src and dst vertices
    Vertex<T>* src_vertex = this->get_vertex(src);
    Vertex<T>* dst_vertex = this->get_vertex(dst);
    if (!src_vertex || !dst_vertex) return path;

    // apply dijkstra's shortest path from src.
    // the following will exhaust all paths in G until the desired, shortest path is found.
    Queue::PriorityQueue<Vertex<T>*, std::greater<std::pair<double, Vertex<T>*>>>* visited = new Queue::PriorityQueue<Vertex<T>*, std::greater<std::pair<double, Vertex<T>*>>>(); // induces a min pqueue data structure.
    src_vertex->cost = 0.0;
    visited->push(src_vertex->cost, src_vertex);

    while (!visited->empty()) {
      Vertex<T>* vertex = visited->top().second;
      visited->pop();
      vertex->visited = true;
      if (vertex == dst_vertex) {
        // cool, now reconstruct dijkstra's shortest path from dst
        for (Vertex<T>* curr = dst_vertex; curr != src_vertex->prev; curr = curr->prev) {
          path.push(curr);
        }
        path.reverse();
        break;
      }

      for (Vertex<T>* neighbor : vertex->neighbors) {
        if (!neighbor->visited) {
          double cost = vertex->cost + this->get_edge(vertex, neighbor)->weight;

          if (cost < neighbor->cost) {
            neighbor->cost = cost;
            neighbor->prev = vertex;
            visited->push(neighbor->cost, neighbor);
          }
        }
      }
    }

    delete visited;
    return path;
  }

  template<typename T>
  Vector::Vector<Vertex<T>*> Graph<T>::astar(const T src, const T dst, const std::function<double (const Vertex<T>* src, const Vertex<T>* dst)>& heuristic) {
    // house keeping
    this->mark_all_vertices_unvisited();
    Vector::Vector<Vertex<T>*> path;

    // inspect src and dst vertices
    Vertex<T>* src_vertex = this->get_vertex(src);
    Vertex<T>* dst_vertex = this->get_vertex(dst);
    if (!src_vertex || !dst_vertex) return path;

    // A* shortest path with an heuristic estimator
    Queue::PriorityQueue<Vertex<T>*, std::greater<std::pair<double, Vertex<T>*>>>* visited = new Queue::PriorityQueue<Vertex<T>*, std::greater<std::pair<double, Vertex<T>*>>>(); // induces a min pqueue data structure.
    src_vertex->cost = 0.0;
    visited->push(src_vertex->cost + heuristic(src_vertex, dst_vertex), src_vertex);

    while (!visited->empty()) {
      Vertex<T>* vertex = visited->top().second;
      visited->pop();
      vertex->visited = true;
      if (vertex == dst_vertex) {
        // cool, now reconstruct A* shortest path from dst
        for (Vertex<T>* curr = dst_vertex; curr != src_vertex->prev; curr = curr->prev) {
          path.push(curr);
        }
        path.reverse();
        break;
      }

      for (Vertex<T>* neighbor : vertex->neighbors) {
        if (!neighbor->visited) {
          double cost = vertex->cost + this->get_edge(vertex, neighbor)->weight;

          if (cost < neighbor->cost) {
            neighbor->cost = cost;
            neighbor->prev = vertex;
            visited->push(neighbor->cost + heuristic(neighbor, dst_vertex), neighbor);
          }
        }
      }
    }

    delete visited;
    return path;
  }

  template<typename T>
  void Graph<T>::mark_all_vertices_unvisited() {
    for (Vertex<T>* vertex : this->vertices) {
      vertex->prev = nullptr;
      vertex->cost = std::numeric_limits<double>::max();
      vertex->visited = false;
    }
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Graph<T>& graph) {
    int index;
    int vertices = graph.vertices.size();
    int edges = graph.edges.size();

    out << "{\n";

    index = 0;
    for (const Vertex<T>* vertex : graph.vertices) {
      out << "  " << vertex << (index == vertices - 1 && edges == 0 ? "" : ", ") << "\n";
      ++index;
    }

    index = 0;
    for (const Edge<T>* edge : graph.edges) {
      out << "  " << edge << (index == edges - 1 ? "" : ", ") << "\n";
      ++index;
    }

    out << "}";
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Graph<T>* graph) {
    int index;
    int vertices = graph->vertices.size();
    int edges = graph->edges.size();

    out << "{\n";

    index = 0;
    for (const Vertex<T>* vertex : graph->vertices) {
      out << "  " << vertex << (index == vertices - 1 && edges == 0 ? "" : ", ") << "\n";
      ++index;
    }

    index = 0;
    for (const Edge<T>* edge : graph->edges) {
      out << "  " << edge << (index == edges - 1 ? "" : ", ") << "\n";
      ++index;
    }

    out << "}";
    return out;
  }
}

#endif

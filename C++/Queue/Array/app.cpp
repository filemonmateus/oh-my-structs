#include <iostream>
#include "lib/Queue.h"

// templated class shortener/alias
using BasicQueue = Queue::QueueArray<int>;

int main() {
  // init queue
  BasicQueue* q = new BasicQueue;

  // add values
  q->push(1);
  q->push(2);
  q->push(3);
  q->push(4);
  q->push(5);

  // inspect queue
  std::cout << q << std::endl;

  // check FIFO invariance
  std::cout << "front() -> " << q->front() << std::endl;
  q->pop();
  std::cout << "front() -> " << q->front() << std::endl;
  q->pop();

  // check queue state
  std::cout << q << std::endl;

  // that's all
  delete q;
}

#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include <cassert>
#include <ostream>

namespace Queue {
  template<typename T>
  class QueueArray {
    private:
      uint head;
      uint tail;
      uint capacity;
      T* elements;

    public:
      QueueArray(const uint capacity=10);
      virtual ~QueueArray();
      void push(const T element);
      void pop();
      void resize();
      T front() const;
      uint size() const;
      bool empty() const;
      bool full() const;
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const QueueArray<__T>& queue);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const QueueArray<__T>* queue);
  };

  template<typename T>
  QueueArray<T>::QueueArray(const uint capacity) {
    assert(capacity > 0 && ":< oops, capacity must be a strictly positive int!");
    this->head = this->tail = 0;
    this->capacity = capacity;
    this->elements = new T[this->capacity];
  }

  template<typename T>
  QueueArray<T>::~QueueArray() {
    delete[] this->elements;
  }

  template<typename T>
  bool QueueArray<T>::full() const {
    return this->size() == this->capacity;
  }

  template<typename T>
  bool QueueArray<T>::empty() const {
    return this->size() == 0;
  }

  template<typename T>
  uint QueueArray<T>::size() const {
    return this->tail - this->head;
  }

  template<typename T>
  void QueueArray<T>::resize() {
    this->capacity *= 2;
    T* elements = new T[this->capacity];
    for (int i = 0; i < this->size(); ++i) {
      elements[i] = this->elements[this->head+i];
    }
    delete[] this->elements;
    this->elements = elements;
  }

  template<typename T>
  void QueueArray<T>::push(const T element) {
    if (this->full()) this->resize();
    this->elements[this->tail++] = element;
  }

  template<typename T>
  T QueueArray<T>::front() const {
    if (this->empty()) throw std::length_error(":< oops, can\'t call front on empty queue!");
    return this->elements[this->head];
  }

  template<typename T>
  void QueueArray<T>::pop() {
    if (this->empty()) throw std::length_error(":< oops, can't call pop on empty queue!");
    this->head++;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const QueueArray<T>& queue) {
    for (int i = queue.head; i < queue.tail; ++i) {
      out << queue.elements[i] << " ";
    }
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const QueueArray<T>* queue) {
    for (int i = queue->head; i < queue->tail; ++i) {
      out << queue->elements[i] << " ";
    }
    return out;
  }
}

#endif

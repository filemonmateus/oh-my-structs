#include <iostream>
#include "lib/Queue.h"

// templated class shortener/alias
using BasicQueue = Queue::QueueList<int>;

int main() {
  // init queue
  BasicQueue* q = new BasicQueue;

  // construct queue
  q->push(1);
  q->push(2);
  q->push(3);
  q->push(4);
  q->push(5);

  // inspect queue
  std::cout << q << std::endl;

  // check FIFO invariance
  std::cout << "front() -> " << q->front() << std::endl;
  q->pop();
  std::cout << "front() -> " << q->front() << std::endl;
  q->pop();

  // inspect queue state
  std::cout << q <<std::endl;

  // that's all
  delete q;
}

#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include "../../../List/DLL/lib/List.h"

namespace Queue {
  template<typename T>
  class QueueList : public List::DoublyLinkedList<T> {
    public:
      QueueList();
      void push(const T key);
      void pop();
      T front() const;
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const QueueList<__T>& queue);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const QueueList<__T>* queue);
  };

  template<typename T>
  QueueList<T>::QueueList() : List::DoublyLinkedList<T>() {}

  template<typename T>
  void QueueList<T>::push(const T key) {
    this->append(key);
  }

  template<typename T>
  void QueueList<T>::pop() {
    if (this->empty()) throw std::length_error(":< oops, can\'t call pop on empty queue!");
    this->remove(this->head->key);
  }

  template<typename T>
  T QueueList<T>::front() const {
    if (this->empty()) throw std::length_error(":< oops, can\'t call front on empty queue!");
    return this->head->key;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const QueueList<T>& queue) {
    for (List::Node<T>* curr = queue.head; curr; curr = curr->next)
      out << curr << " ";
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const QueueList<T>* queue) {
    for (List::Node<T>* curr = queue->head; curr; curr = curr->next)
      out << curr << " ";
    return out;
  }
}

#endif

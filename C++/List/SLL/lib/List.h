#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include "Node.h"

namespace List {
  template<typename T>
  class SinglyLinkedList {
    protected:
      uint msize;
      Node<T>* head;

    public:
      SinglyLinkedList();
      virtual ~SinglyLinkedList();
      void prepend(const T key);
      void append(const T key);
      void remove(const T key);
      bool lookup(const T key) const;
      bool empty() const;
      uint size() const;
      void reverse();
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const SinglyLinkedList<__T>& list);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const SinglyLinkedList<__T>* list);
  };

  template<typename T>
  SinglyLinkedList<T>::SinglyLinkedList() {
    this->msize = 0;
    this->head = nullptr;
  }

  template<typename T>
  SinglyLinkedList<T>::~SinglyLinkedList() {
    if (this->empty()) return;
    Node<T>* curr = this->head;
    Node<T>* succ = nullptr;

    while (curr) {
      succ = curr->next;
      delete curr;
      curr = succ;
    }

    this->msize = 0;
  }

  template<typename T>
  void SinglyLinkedList<T>::prepend(const T key) {
    Node<T>* node = new Node<T>(key);
    node->next = this->head;
    this->head = node;
    this->msize++;
  }

  template<typename T>
  void SinglyLinkedList<T>::append(const T key) {
    if (this->empty()) return this->prepend(key);
    Node<T>* curr = this->head->next;
    Node<T>* pred = this->head;

    while (curr) {
      pred = curr;
      curr = curr->next;
    }

    pred->next = curr = new Node<T>(key);
    this->msize++;
  }

  template<typename T>
  void SinglyLinkedList<T>::remove(const T key) {
    if (this->empty()) {
      return;
    }
    else if (this->head->key == key) {
      Node<T>* trash = this->head;
      this->head = this->head->next;
      delete trash;
      this->msize--;
      return;
    }

    Node<T>* trash = this->head->next;
    Node<T>* pred = this->head;

    while (trash) {
      if (trash->key == key) {
        pred->next = trash->next;
        delete trash;
        this->msize--;
        return;
      }

      pred = trash;
      trash = trash->next;
    }
  }

  template<typename T>
  bool SinglyLinkedList<T>::lookup(const T key) const {
    for (Node<T>* curr = this->head; curr != nullptr; curr = curr->next) {
      if (curr->key == key) {
        return true;
      }
    }
    return false;
  }

  template<typename T>
  bool SinglyLinkedList<T>::empty() const {
    return this->msize == 0;
  }

  template<typename T>
  uint SinglyLinkedList<T>::size() const {
    return this->msize;
  }

  template<typename T>
  void SinglyLinkedList<T>::reverse() {
    if (this->msize < 2) return;
    Node<T>* curr = this->head;
    Node<T>* succ = nullptr;
    Node<T>* pred = nullptr;

    while (curr) {
      succ = curr->next;
      curr->next= pred;
      pred = curr;
      curr = succ;
    }

    this->head = pred;
  }
  
  template<typename T>
  std::ostream& operator << (std::ostream& out, const SinglyLinkedList<T>& list) {
    for (Node<T>* curr = list.head; curr; curr = curr->next)
      out << curr << (curr->next ? " " : "");
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const SinglyLinkedList<T>* list) {
    for (Node<T>* curr = list->head; curr; curr = curr->next)
      out << curr << (curr->next ? " " : "");
    return out;
  }
}

#endif

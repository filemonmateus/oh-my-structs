#include <iostream>
#include "lib/List.h"

// templated class shortener/alias
using SLL = List::SinglyLinkedList<int>;

int main() {
  // init list
  SLL* ints_mod_five = new SLL;

  // append ints mod 5
  ints_mod_five->append(0);
  ints_mod_five->append(1);
  ints_mod_five->append(2);
  ints_mod_five->append(3);
  ints_mod_five->append(4);

  // inspect integers mod 5
  std::cout << ints_mod_five << std::endl;

  // that's all
  delete ints_mod_five;
}

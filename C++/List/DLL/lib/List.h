#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include "Node.h"

namespace List {
  template<typename T>
  class DoublyLinkedList {
    protected:
      uint msize;
      Node<T>* head;
      Node<T>* tail;

    public:
      DoublyLinkedList();
      virtual ~DoublyLinkedList();
      void prepend(const T key);
      void append(const T key);
      void remove(const T key);
      bool lookup(const T key) const;
      bool empty() const;
      uint size() const;
      void reverse();
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const DoublyLinkedList<__T>& list);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const DoublyLinkedList<__T>* list);
  };

  template<typename T>
  DoublyLinkedList<T>::DoublyLinkedList() {
    this->msize = 0;
    this->head = this->tail = nullptr;
  }

  template<typename T>
  DoublyLinkedList<T>::~DoublyLinkedList() {
    if (this->empty()) return;
    Node<T>* curr = this->head;
    Node<T>* succ = nullptr;

    while (curr) {
      succ = curr->next;
      delete curr;
      curr = succ;
    }

    this->msize = 0;
  }

  template<typename T>
  void DoublyLinkedList<T>::prepend(const T key) {
    if (this->empty()) {
      this->head = this->tail = new Node<T>(key);
      this->msize++;
      return;
    }

    Node<T>* node = new Node<T>(key);
    node->next = this->head;
    this->head->prev = node;
    this->head = node;
    this->msize++;
  }

  template<typename T>
  void DoublyLinkedList<T>::append(const T key) {
    if (this->empty()) return this->prepend(key);
    Node<T>* node = new Node<T>(key);
    node->prev = this->tail;
    this->tail->next = node;
    this->tail = node;
    this->msize++;
  }

  template<typename T>
  void DoublyLinkedList<T>::remove(const T key) {
    if (this->empty()) {
      return;
    }
    else if (this->head->key == key) {
      Node<T>* trash = this->head;
      this->head = this->head->next;

      if (this->head) {
        this->head->prev = nullptr;
      }

      delete trash;
      this->msize--;
      return;
    }
    else if (this->tail->key == key) {
      Node<T>* trash = this->tail;
      this->tail = this->tail->prev;
      this->tail->next = trash->next;
      delete trash;
      this->msize--;
      return;
    }

    for (Node<T>* trash = this->head->next; trash != this->tail; trash = trash->next) {
      if (trash->key == key) {
        trash->prev->next = trash->next;
        trash->next->prev = trash->prev;
        delete trash;
        this->msize--;
        return;
      }
    }
  }

  template<typename T>
  bool DoublyLinkedList<T>::lookup(const T key) const {
    for (Node<T>* curr = this->head; curr != nullptr; curr = curr->next) {
      if (curr->key == key) {
        return true;
      }
    }
    return false;
  }

  template<typename T>
  bool DoublyLinkedList<T>::empty() const {
    return this->msize == 0;
  }

  template<typename T>
  uint DoublyLinkedList<T>::size() const {
    return this->msize;
  }

  template<typename T>
  void DoublyLinkedList<T>::reverse() {
    if (this->msize < 2) {
      return;
    }

    Node<T>* curr = this->head;
    Node<T>* pred = nullptr;

    while (curr) {
      pred = curr->prev;
      curr->prev = curr->next;
      curr->next = pred;
      curr = curr->prev;
    }

    this->head = this->tail;
    this->tail = pred->prev;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const DoublyLinkedList<T>& list) {
    for (Node<T>* curr = list.head; curr; curr = curr->next)
      out << curr << (curr->next ? " " : "");
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const DoublyLinkedList<T>* list) {
    for (Node<T>* curr = list->head; curr; curr = curr->next)
      out << curr << (curr->next ? " " : "");
    return out;
  }
}

#endif

#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <ostream>

namespace List {
  template<typename T>
  class Node {
    public:
      T key;
      Node<T>* prev;
      Node<T>* next;
      Node(const T key);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Node<__T>& node);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const Node<__T>* node);
  };

  template<typename T>
  Node<T>::Node(const T key) {
    this->key = key;
    this->prev = this->next = nullptr;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Node<T>& node) {
    out << node.key; return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const Node<T>* node) {
    out << node->key; return out;
  }
}

#endif

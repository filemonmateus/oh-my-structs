#include <iostream>
#include "lib/List.h"

// templated class shortener/alias
using DLL = List::DoublyLinkedList<int>;

int main() {
  // init primes
  DLL* primes = new DLL;

  // add a few
  primes->append(2);
  primes->append(3);
  primes->append(5);
  primes->append(7);

  // inspect them
  std::cout << primes << std::endl;

  // that's all
  delete primes;
}

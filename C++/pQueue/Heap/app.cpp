#include <iostream>
#include "lib/pQueue.h"
#include "../../Vector/lib/Vector.h"

// a general point struct
struct Point {
  double x, y, d;
  Point(const double x, const double y) : x(x), y(y), d(x*x + y*y) {};
};

// templated class shorterner/alias
using BasicVector = Vector::Vector<Point*>;
using PriorityQueue = Queue::PriorityQueue<Point*>; // [info] defaults to a max pqueue

// function prototypes / interfaces
BasicVector* kclosests(const BasicVector* points, const int k);
std::ostream& operator << (std::ostream& out, const Point* point);

int main() {
  BasicVector* points = new BasicVector;
  points->push(new Point(4,4));
  points->push(new Point(1,1));
  points->push(new Point(6,6));
  points->push(new Point(2,2));
  points->push(new Point(0,0));
  points->push(new Point(3,3));
  points->push(new Point(5,5));
  std::cout << points << std::endl;

  BasicVector* closests = kclosests(points, 2);
  std::cout << closests << std::endl;
  
  for (int i = 0; i < points->size(); ++i) delete points->get(i);
  delete points;
  delete closests;
}

// returns k closest points from the origin
BasicVector* kclosests(const BasicVector* points, const int k) {
  assert(k > 0);
  if (points->size() <= k) return (BasicVector*) points;
  PriorityQueue* maxheap = new PriorityQueue;
  BasicVector* closests = new BasicVector(k);

  for (int i = 0; i < k; ++i) {
    maxheap->push(points->get(i)->d, points->get(i));
  }
  
  for (int i = k; i < points->size(); ++i) {
    if (points->get(i)->d < maxheap->top().first) {
      maxheap->pop();
      maxheap->push(points->get(i)->d, points->get(i));
    }
  }

  while (!maxheap->empty()) {
    closests->push(maxheap->top().second);
    maxheap->pop();
  }

  delete maxheap;
  return closests;
}

// adds << support for the general point class interface
std::ostream& operator << (std::ostream& out, const Point* point) {
  out << "(" << point->x << "," << point->y << ")";
  return out;
}

#ifndef PQUEUE_H_INCLUDED
#define PQUEUE_H_INCLUDED

#include "../../../Heap/lib/Heap.h"

namespace Queue {
  template<typename T, typename Comparator=std::less<std::pair<double, T>>> // implements by default a max pqueue (a min pqueue is induced with a `Comparator=std::greater<std::pair<double, T>>` typename)
  class PriorityQueue : public Heap::Heap<std::pair<double, T>, Comparator> {
    public:
      PriorityQueue(const uint capacity=10);
      void push(const double priority, const T element);
      void pop();
      std::pair<double, T> top();
      template<typename __T, typename __Comparator> friend std::ostream& operator << (std::ostream& out, const PriorityQueue<__T, __Comparator>& pqueue);
      template<typename __T, typename __Comparator> friend std::ostream& operator << (std::ostream& out, const PriorityQueue<__T, __Comparator>* pqueue);
  };

  template<typename T, typename Comparator>
  PriorityQueue<T, Comparator>::PriorityQueue(const uint capacity) : Heap::Heap<std::pair<double, T>, Comparator>(capacity) {}

  template<typename T, typename Comparator>
  void PriorityQueue<T, Comparator>::push(const double priority, const T element) {
    Heap::Heap<std::pair<double, T>, Comparator>::push({ priority, element });
  }

  template<typename T, typename Comparator>
  void PriorityQueue<T, Comparator>::pop() {
    Heap::Heap<std::pair<double, T>, Comparator>::pop();
  }

  template<typename T, typename Comparator>
  std::pair<double, T> PriorityQueue<T, Comparator>::top() {
    return Heap::Heap<std::pair<double, T>, Comparator>::top();
  }

  template<typename T, typename Comparator>
  std::ostream& operator << (std::ostream& out, const PriorityQueue<T, Comparator>& pqueue) {
    for (int i = 0; i < pqueue.size(); ++i)
      out << "(" << pqueue.get(i).first << "," << pqueue.get(i).second << ")";
    return out;
  }

  template<typename T, typename Comparator>
  std::ostream& operator << (std::ostream& out, const PriorityQueue<T, Comparator>* pqueue) {
    for (int i = 0; i < pqueue->size(); ++i)
      out << "(" << pqueue->get(i).first << "," << pqueue->get(i).second << ") ";
    return out;
  }
};

#endif

#ifndef PQUEUE_H_INCLUDED
#define PQUEUE_H_INCLUDED

#include "../../../List/SLL/lib/List.h"

namespace Queue {
  template<typename T, typename Comparator=std::less<std::pair<double, T>>> // implements by default a max pqueue (a min pqueue is induced with a `Comparator=std::greater<std::pair<double, T>>` typename)
  class PriorityQueue : public List::SinglyLinkedList<std::pair<double, T>> {
    public:
      PriorityQueue();
      void push(const double priority, const T element);
      void pop();
      std::pair<double, T> top() const;
      template<typename __T, typename __Comparator> friend std::ostream& operator << (std::ostream& out, const PriorityQueue<__T, __Comparator>& pqueue);
      template<typename __T, typename __Comparator> friend std::ostream& operator << (std::ostream& out, const PriorityQueue<__T, __Comparator>* pqueue);

    private:
      Comparator compare;
  };

  template<typename T, typename Comparator>
  PriorityQueue<T, Comparator>::PriorityQueue() : List::SinglyLinkedList<std::pair<double, T>>() {
    this->compare = Comparator();
  }

  template<typename T, typename Comparator>
  void PriorityQueue<T, Comparator>::push(const double priority, const T element) {
    std::pair<double, T> new_key({ priority, element });
    if (this->empty() || this->compare(this->head->key, new_key)) return this->prepend(new_key);
    List::Node<std::pair<double, T>>* curr = this->head->next;
    List::Node<std::pair<double, T>>* pred = this->head;

    while (curr) {
      if (!this->compare(curr->key, new_key)) {
        pred = curr;
        curr = curr->next;
      }
      else {
        List::Node<std::pair<double, T>>* node = new List::Node<std::pair<double, T>>(new_key);
        pred->next = node;
        node->next = curr;
        this->msize++;
        return;
      }
    }

    this->append(new_key);
  }

  template<typename T, typename Comparator>
  void PriorityQueue<T, Comparator>::pop() {
    this->remove(this->head->key);
  }

  template<typename T, typename Comparator>
  std::pair<double, T> PriorityQueue<T, Comparator>::top() const {
    return this->head->key;
  }
  
  template<typename T, typename Comparator>
  std::ostream& operator << (std::ostream& out, const PriorityQueue<T, Comparator>& pqueue) {
    for (List::Node<std::pair<double, T>>* curr = pqueue.head; curr; curr = curr->next)
      out << "(" << curr->key.first << "," << curr->key.second << ") ";
    return out;
  }

  template<typename T, typename Comparator>
  std::ostream& operator << (std::ostream& out, const PriorityQueue<T, Comparator>* pqueue) {
    for (List::Node<std::pair<double, T>>* curr = pqueue->head; curr; curr = curr->next)
      out << "(" << curr->key.first << "," << curr->key.second << ") ";
    return out;
  }
}

#endif

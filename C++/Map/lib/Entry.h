#ifndef HASH_H_INCLUDED
#define HASH_H_INCLUDED

#include <ostream>

namespace Hash {
  template<typename K, typename T>
  class Entry {
    public:
      K key;
      T val;
      Entry<K,T>* next;
      Entry(const K key, const T val);
      virtual ~Entry(void);
      template<typename __K, typename __V> friend std::ostream& operator << (std::ostream& out, const Entry<__K, __V>& entry);
      template<typename __K, typename __V> friend std::ostream& operator << (std::ostream& out, const Entry<__K, __V>* entry);
  };

  template<typename K, typename T>
  Entry<K,T>::Entry(const K key, const T val) {
    this->key = key;
    this->val = val;
    this->next = nullptr;
  }

  template<typename K, typename T>
  Entry<K,T>::~Entry(void) {}

  template<typename K, typename T>
  std::ostream& operator << (std::ostream& out, const Entry<K,T>& entry) {
    out << "{" << entry.key << ": " << entry.val << "}"; return out;
  }

  template<typename K, typename T>
  std::ostream& operator << (std::ostream& out, const Entry<K,T>* entry) {
    out << "{" << entry->key << ": " << entry->val << "}"; return out;
  }
};

#endif

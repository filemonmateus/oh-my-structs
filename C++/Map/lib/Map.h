#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#include <ostream>
#include <cassert>

#include "Entry.h"

namespace Hash {
  template<typename K, typename T>
  class Map {
    public:
      Map (const uint capacity=10);
      virtual ~Map ();
      uint size () const;
      void insert (const std::pair<K,T>& entry);
      bool empty () const;
      bool lookup (const K key) const;
      const T query (const K key) const;
      template<typename __K, typename __V> friend std::ostream& operator << (std::ostream& out, const Map<__K, __V>& map);
      template<typename __K, typename __V> friend std::ostream& operator << (std::ostream& out, const Map<__K, __V>* map);

    private:
      uint msize;
      uint capacity;
      std::hash<K> hasher;
      uint hash (const K key) const;
      Entry<K,T>** entries;
  };

  template<typename K, typename T>
  Map<K,T>::Map (const uint capacity) {
    assert(capacity > 0 && ":< oops, capacity must be a strictly positive int!");
    this->msize = 0;
    this->capacity = capacity;
    this->entries = new Entry<K,T>*[this->capacity];
    for (int i = 0; i < this->capacity; ++i) {
      this->entries[i] = nullptr;
    }
  }

  template<typename K, typename T>
  Map<K,T>::~Map () {
    for (int i = 0; i < this->capacity; ++i) {
      Entry<K,T>* curr = this->entries[i];
      Entry<K,T>* succ = nullptr;
      while (curr) {
        succ = curr->next;
        delete curr;
        curr = succ;
      }
    }
    delete[] this->entries;
  }

  template<typename K, typename T>
  uint Map<K,T>::hash (const K key) const {
    return this->hasher(key) % this->capacity;
  }

  template<typename K, typename T>
  uint Map<K,T>::size () const {
    return this->msize;
  }

  template<typename K, typename T>
  void Map<K,T>::insert (const std::pair<K,T>& entry) {
    if (this->entries[this->hash(entry.first)] == nullptr) {
      this->entries[this->hash(entry.first)] = new Entry<K,T>(entry.first, entry.second);
      this->msize++;
      return;
    }

    Entry<K,T>* curr = this->entries[this->hash(entry.first)];
    Entry<K,T>* pred = nullptr;

    while (curr) {
      // update preexisting (key, val) in chaining
      if (curr->key == entry.first) {
        curr->val = entry.second;
        return;
      }
      pred = curr;
      curr = curr->next;
    }

    // insert new (key, val) in chaining
    pred->next = curr = new Entry<K,T>(entry.first, entry.second);
    this->msize++;
  }

  template<typename K, typename T>
  bool Map<K,T>::empty () const  {
    return this->msize == 0;
  }

  template<typename K, typename T>
  bool Map<K,T>::lookup (const K key) const {
    for (Entry<K,T>* entry = this->entries[this->hash(key)]; entry != nullptr; entry = entry->next) {
      if (entry->key == key) {
        return true;
      }
    } 
    return false;
  }

  template<typename K, typename T>
  const T Map<K,T>::query (const K key) const {
    for (Entry<K,T>* entry = this->entries[this->hash(key)]; entry != nullptr; entry = entry->next) {
      if (entry->key == key) {
        return entry->val;
      }
    }
    throw std::range_error(":< oops, permissible search space exhausted: key '" + key + "' not found!");
  }

  template<typename K, typename T>
  std::ostream& operator << (std::ostream& out, const Map<K,T>& map) {
    for (int i = 0; i < map.capacity; ++i) {
      if (map.entries[i]) {
        out << i << ". " << map.entries[i] << "->";
        for (Entry<K,T>* entry = map.entries[i]->next; entry != nullptr; entry = entry->next)  {
          out << entry << "->";
        }
        out << "null\n";
      }
    }
    return out;
  }

  template<typename K, typename T>
  std::ostream& operator << (std::ostream& out, const Map<K,T>* map) {
    for (int i = 0; i < map->capacity; ++i) {
      if (map->entries[i]) {
        out << i << ". " << map->entries[i] << "->"; 
        for (Entry<K,T>* entry =  map->entries[i]->next; entry != nullptr; entry = entry->next) {
          out << entry  << "->";
        }
        out << "null\n";
      }
    }
    return out;
  } 
}

#endif

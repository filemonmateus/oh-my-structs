#include <iostream>
#include "lib/Map.h"

// templated class shortener/alias
using HashMap = Hash::Map<std::string, double>;

int main() {
  HashMap* currencies = new HashMap(2); 
  currencies->insert({"eur", 1.11});
  currencies->insert({"usd", 0.99});
  currencies->insert({"gbp", 1.32});
  currencies->insert({"yen", 9.16});
  std::cout << currencies;
  std::cout << currencies->query("eur") << std::endl;
  std::cout << currencies->lookup("eur") << std::endl;
  delete currencies;
}

#include <iostream>
#include "lib/BstTree.h"

// templated class shortener/alias
using BST = Tree::BstTree<int>;

int main() {
  // init tree
  BST* tree = new BST;

  // construct tree
  tree->insert(10);
  tree->insert(06);
  tree->insert(14);
  tree->insert(04);
  tree->insert(07);
  tree->insert(12);
  tree->insert(16);

  // inspect tree
  std::cout << tree->preorder() << std::endl;
  std::cout << tree->inorder() << std::endl;
  std::cout << tree->postorder() << std::endl;
  std::cout << tree->levelorder() << std::endl;

  // that's all
  delete tree;
}

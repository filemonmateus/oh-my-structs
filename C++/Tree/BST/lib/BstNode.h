#ifndef BST_NODE_H_INCLUDED
#define BST_NODE_H_INCLUDED

#include <ostream>

namespace Tree {
  template<typename T>
  class BstNode {
    public:
      T key;
      BstNode* left;
      BstNode* right;
      BstNode(const T key);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const BstNode<__T>& node);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const BstNode<__T>* node);
  };

  template<typename T>
  BstNode<T>::BstNode(const T key) {
    this->key = key;
    this->left = this->right = nullptr;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const BstNode<T>& node) {
    out << node.key; return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const BstNode<T>* node) {
    out << node->key; return out;
  }
}

#endif

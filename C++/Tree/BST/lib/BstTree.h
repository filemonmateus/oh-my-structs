#ifndef BST_TREE_H_INCLUDED
#define BST_TREE_H_INCLUDED

#include "BstNode.h"
#include "../../../Vector/lib/Vector.h"
#include "../../../Stack/Array/lib/Stack.h"
#include "../../../Queue/Array/lib/Queue.h"

namespace Tree {
  template<typename T>
  class BstTree {
    public:
      BstTree ();
      virtual ~BstTree ();
      void insert (const T key);
      void remove (const T key);
      bool lookup (const T key) const;
      bool empty () const;
      uint size () const;
      uint height () const;
      Vector::Vector<BstNode<T>*> preorder () const;
      Vector::Vector<BstNode<T>*> inorder () const;
      Vector::Vector<BstNode<T>*> postorder () const;
      Vector::Vector<BstNode<T>*> levelorder () const;
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const BstTree<__T>& tree);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const BstTree<__T>* tree);

    private:
      uint msize;
      BstNode<T>* root;
      BstNode<T>* _remove (BstNode<T>* curr, const T key);
      BstNode<T>* _minimum (BstNode<T>* curr) const;
      BstNode<T>* _maximum (BstNode<T>* curr) const;
      uint _height (const BstNode<T>* curr) const;
      void _clear (BstNode<T>* curr);
  };

  template<typename T>
  BstTree<T>::BstTree () {
    this->msize = 0;
    this->root = nullptr;
  }

  template<typename T>
  BstTree<T>::~BstTree () {
    if (this->empty()) return;
    this->_clear(this->root);
    this->msize = 0;
  }

  template<typename T>
  void BstTree<T>::_clear (BstNode<T>* curr) {
    if (curr == nullptr) return;
    this->_clear(curr->left);
    this->_clear(curr->right);
    delete curr;
  }

  template<typename T>
  void BstTree<T>::insert (const T key) {
    if (this->empty()) {
      this->root = new BstNode<T>(key);
      this->msize++;
      return;
    }

    BstNode<T>* curr = this->root;

    while (curr) {
      if (key < curr->key) {
        if (curr->left) {
          curr = curr->left;
        }
        else {
          curr->left = new BstNode<T>(key);
          this->msize++;
          return;
        }
      }
      else {
        if (curr->right) {
          curr = curr->right;
        }
        else {
          curr->right = new BstNode<T>(key);
          this->msize++;
          return;
        }
      }
    }
  }

  template<typename T>
  BstNode<T>* BstTree<T>::_minimum (BstNode<T>* curr) const {
    return curr->left ? this->_minimum(curr->left) : curr;
  }

  template<typename T>
  BstNode<T>* BstTree<T>::_maximum (BstNode<T>* curr) const {
    return curr->right ? this->_maximum(curr->right) : curr;
  }

  template<typename T>
  BstNode<T>* BstTree<T>::_remove (BstNode<T>* curr, const T key) {
    if (curr == nullptr) return curr;
    else if (key < curr->key) curr->left = this->_remove(curr->left, key);
    else if (key > curr->key) curr->right = this->_remove(curr->right, key);
    else {
      // case 0 children -> leaf node
      if (curr->left == nullptr && curr->right == nullptr) {
        BstNode<T>* trash = curr;
        curr = nullptr;
        delete trash;
      }
      // case 1 child
      else if (curr->left == nullptr) {
        BstNode<T>* trash = curr;
        curr = curr->right;
        delete trash;
      }
      else if (curr->right == nullptr) {
        BstNode<T>* trash = curr;
        curr = curr->left;
        delete trash;
      }
      // case 2 children
      else {
        BstNode<T>* trash = this->_minimum(curr->right);
        curr->key = trash->key;
        curr->right = this->_remove(curr->right, curr->key);
      }
    }

    return curr;
  }

  template<typename T>
  void BstTree<T>::remove (const T key) {
    this->root = this->_remove(this->root, key);
    this->msize--;
  }

  template<typename T>
  bool BstTree<T>::lookup (const T key) const {
    if (this->empty()) return false;
    BstNode<T>* curr = this->root;

    while (curr) {
      if (curr->key == key) return true;
      else if (curr->key > key) curr = curr->left;
      else curr = curr->right;
    }

    return false;
  }

  template<typename T>
  bool BstTree<T>::empty () const {
    return this->msize == 0;
  }

  template<typename T>
  uint BstTree<T>::size () const {
    return this->msize;
  }

  template<typename T>
  uint BstTree<T>::_height (const BstNode<T>* curr) const {
    return curr ? std::max(this->_height(curr->left), this->_height(curr->right)) + 1 : 0;
  }

  template<typename T>
  uint BstTree<T>::height () const {
    return this->_height(this->root);
  }

  template<typename T>
  Vector::Vector<BstNode<T>*> BstTree<T>::preorder () const {
    Vector::Vector<BstNode<T>*> path;
    if (this->empty()) return path;
    BstNode<T>* curr = this->root;
    Stack::StackArray<BstNode<T>*>* s = new Stack::StackArray<BstNode<T>*>(this->msize);
    s->push(curr);

    while (!s->empty()) {
      curr = s->top();
      s->pop();
      path.push(curr);
      if (curr->right) s->push(curr->right);
      if (curr->left) s->push(curr->left);
    }

    delete s;
    return path;
  }

  template<typename T>
  Vector::Vector<BstNode<T>*> BstTree<T>::inorder () const {
    Vector::Vector<BstNode<T>*> path;
    if (this->empty()) return path;
    BstNode<T>* curr = this->root;
    Stack::StackArray<BstNode<T>*>* s = new Stack::StackArray<BstNode<T>*>(this->msize);

    while (!s->empty() || curr) {
      if (curr) {
        s->push(curr);
        curr = curr->left;
      }
      else {
        curr = s->top();
        s->pop();
        path.push(curr);
        curr = curr->right;
      }
    }

    delete s;
    return path;
  }

  template<typename T>
  Vector::Vector<BstNode<T>*> BstTree<T>::postorder () const {
    Vector::Vector<BstNode<T>*> path;
    if (this->empty()) return path;
    BstNode<T>* curr = this->root;
    Stack::StackArray<BstNode<T>*>* s = new Stack::StackArray<BstNode<T>*>(this->msize);
    Stack::StackArray<BstNode<T>*>* t = new Stack::StackArray<BstNode<T>*>(this->msize);
    s->push(curr);

    while (!s->empty()) {
      curr = s->top();
      s->pop();
      t->push(curr);
      if (curr->left) s->push(curr->left);
      if (curr->right) s->push(curr->right);
    }

    while (!t->empty()) {
      path.push(t->top());
      t->pop();
    }

    delete s;
    delete t;
    return path;
  }

  template<typename T>
  Vector::Vector<BstNode<T>*> BstTree<T>::levelorder () const {
    Vector::Vector<BstNode<T>*> path;
    if (this->empty()) return path;
    BstNode<T>* curr = this->root;
    Queue::QueueArray<BstNode<T>*>* q = new Queue::QueueArray<BstNode<T>*>(this->msize);
    q->push(curr);

    while (!q->empty()) {
      curr = q->front();
      q->pop();
      path.push(curr);
      if (curr->left) q->push(curr->left);
      if (curr->right) q->push(curr->right);
    }

    delete q;
    return path;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const BstTree<T>& tree) {
    Vector::Vector<BstNode<T>*> path = tree.inorder();
    std::copy(path.begin(), path.end(), std::ostream_iterator<BstNode<T>*>(out, " "));
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const BstTree<T>* tree) {
    Vector::Vector<BstNode<T>*> path = tree->inorder();
    std::copy(path.begin(), path.end(), std::ostream_iterator<BstNode<T>*>(out, " "));
    return out;
  }
}

#endif

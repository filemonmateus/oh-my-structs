#ifndef RBT_TREE_H_INCLUDED
#define RBT_TREE_H_INCLUDED

#include "RbtNode.h"
#include "../../../Vector/lib/Vector.h"
#include "../../../Stack/Array/lib/Stack.h"
#include "../../../Queue/Array/lib/Queue.h"

namespace Tree {
  template<typename T>
  class RbtTree {
    public:
      RbtTree();
      virtual ~RbtTree();
      uint height() const;
      bool empty() const;
      uint size() const;
      void insert(const T key);
      void remove(const T key);
      RbtNode<T>* lookup(const T key) const;
      Vector::Vector<RbtNode<T>*> preorder() const;
      Vector::Vector<RbtNode<T>*> inorder() const;
      Vector::Vector<RbtNode<T>*> postorder() const;
      Vector::Vector<RbtNode<T>*> levelorder() const;
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const RbtTree<__T>& tree);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const RbtTree<__T>* tree);

    private:
      uint msize;
      RbtNode<T>* null;
      RbtNode<T>* root;
      RbtNode<T>* minimum(RbtNode<T>* current) const;
      RbtNode<T>* maximum(RbtNode<T>* current) const;
      RbtNode<T>* successor(RbtNode<T>* current) const;
      RbtNode<T>* predecessor(RbtNode<T>* current) const;
      void clear(RbtNode<T>* current);
      void insert_fixup(RbtNode<T>* current);
      void remove_fixup(RbtNode<T>* current);
      void left_rotate(RbtNode<T>* current);
      void right_rotate(RbtNode<T>* current);
      void transplant(RbtNode<T>* this_node, RbtNode<T>* that_node);
      uint height(RbtNode<T>* current) const;
  };

  template<typename T>
  RbtTree<T>::RbtTree() {
    this->msize = 0;
    this->root = this->null = new RbtNode<T>(0, BLACK);
  }

  template<typename T>
  RbtTree<T>::~RbtTree() {
    if (this->empty()) return;
    this->clear(this->root);
    delete this->null;
    this->msize = 0;
  }

  template<typename T>
  void RbtTree<T>::clear(RbtNode<T>* current) {
    if (current == this->null) return;
    this->clear(current->left);
    this->clear(current->right);
    delete current;
  }

  template<typename T>
  uint RbtTree<T>::height() const {
    return this->height(this->root);
  }

  template<typename T>
  uint RbtTree<T>::height(RbtNode<T>* current) const {
    return current == nullptr || current == this->null ? 0 : std::max(this->height(current->left), this->height(current->right)) + 1;
  }

  template<typename T>
  bool RbtTree<T>::empty() const {
    return this->msize == 0;
  }

  template<typename T>
  uint RbtTree<T>::size() const {
    return this->msize;
  }

  template<typename T>
  RbtNode<T>* RbtTree<T>::minimum(RbtNode<T>* current) const {
    return current->left ? this->minimum(current->left) : current;
  }

  template<typename T>
  RbtNode<T>* RbtTree<T>::maximum(RbtNode<T>* current) const {
    return current->right ? this->maximum(current->right) : current;
  }

  template<typename T>
  RbtNode<T>* RbtTree<T>::successor(RbtNode<T>* current) const {
    if (current->right) return this->minimum(current->right);
    RbtNode<T>* parent = current->parent;
    while (parent && current == parent->right) {
      current = parent;
      parent = parent->parent;
    }
    return parent;
  }

  template<typename T>
  RbtNode<T>* RbtTree<T>::predecessor(RbtNode<T>* current) const {
    if (current->left) return this->maximum(current->left);
    RbtNode<T>* parent = current->parent;
    while (parent && current == parent->left) {
      current = parent;
      parent = parent->parent;
    }
    return parent;
  }

  template<typename T>
  void RbtTree<T>::transplant(RbtNode<T>* this_node, RbtNode<T>* that_node) {
    if (this_node->parent == this->null) this->root = that_node;
    else if (this_node == this_node->parent->left) this_node->parent->left = that_node;
    else this_node->parent->right = that_node;
    that_node->parent = this_node->parent;
  }

  template<typename T>
  void RbtTree<T>::left_rotate(RbtNode<T>* current) {
    RbtNode<T>* child = current->right;
    current->right = child->left;
    if (child->left != this->null) child->left->parent = current;
    child->parent = current->parent;
    if (current->parent == this->null) this->root = child;
    else if (current == current->parent->left) current->parent->left = child;
    else current->parent->right = child;
    child->left = current;
    current->parent = child;
  }

  template<typename T>
  void RbtTree<T>::right_rotate(RbtNode<T>* current) {
    RbtNode<T>* child = current->left;
    current->left = child->right;
    if (child->right != this->null) child->right->parent = current;
    child->parent = current->parent;
    if (current->parent == this->null) this->root = child;
    else if (current == current->parent->right) current->parent->right = child;
    else current->parent->left = child;
    child->right = current;
    current->parent = child;
  }

  template<typename T>
  void RbtTree<T>::insert(const T key) {
    RbtNode<T>* parent = this->null;
    RbtNode<T>* current = this->root;
    RbtNode<T>* newnode = new RbtNode<T>(key);

    while (current != this->null) {
      parent = current;
      if (newnode->key < current->key) current = current->left;
      else current = current->right;
    }

    newnode->parent = parent;
    if (parent == this->null) this->root = newnode;
    else if (newnode->key < parent->key) parent->left = newnode;
    else parent->right = newnode;
    newnode->left = this->null;
    newnode->right = this->null;
    newnode->color = RED;
    this->insert_fixup(newnode);
  }

  template<typename T>
  void RbtTree<T>::insert_fixup(RbtNode<T>* current) {
    while (current->parent->color == RED) {
      if (current->parent == current->parent->parent->left) {
        RbtNode<T>* uncle = current->parent->parent->right;

        if (uncle->color == RED) {
          current->parent->color = BLACK;
          uncle->color = BLACK;
          current->parent->parent->color = RED;
          current = current->parent->parent;
        }
        else {
          if (current == current->parent->right) {
            current = current->parent;
            this->left_rotate(current);
          }

          current->parent->color = BLACK;
          current->parent->parent->color = RED;
          this->right_rotate(current->parent->parent);
        }
      }
      else {
        RbtNode<T>* uncle = current->parent->parent->left;

        if (uncle->color == RED) {
          current->parent->color = BLACK;
          uncle->color = BLACK;
          current->parent->parent->color = RED;
          current = current->parent->parent;
        }
        else {
          if (current == current->parent->left) {
            current = current->parent;
            this->right_rotate(current);
          }

          current->parent->color = BLACK;
          current->parent->parent->color = RED;
          this->left_rotate(current->parent->parent);
        }
      }
    }

    this->root->color = BLACK;
    this->msize++;
  }

  template<typename T>
  void RbtTree<T>::remove(const T key) {
    RbtNode<T>* trash = this->lookup(key);
    if (!trash) return;
    RbtNode<T>* trash_copy = trash;
    RbtNode<T>* child;
    enum COLOR trash_copy_original_color = trash_copy->color;

    if (trash->left == this->null) {
      child = trash->right;
      this->transplant(trash, trash->right);
    }
    else if (trash->right == this->null) {
      child = trash->left;
      this->transplant(trash, trash->left);
    }
    else {
      trash_copy = this->minimum(trash->right);
      trash_copy_original_color = trash_copy->color;
      child = trash_copy->right;

      if (trash_copy->parent == trash) {
        child->parent = trash;
      }
      else {
        this->transplant(trash_copy, trash_copy->right);
        trash_copy->right = trash->right;
        trash_copy->right->parent = trash_copy;
      }

      this->transplant(trash, trash_copy);
      trash_copy->left = trash->left;
      trash_copy->left->parent = trash_copy;
      trash_copy->color = trash->color;
    }

    if (trash_copy_original_color == BLACK) {
      this->remove_fixup(child);
    }
  }

  template<typename T>
  void RbtTree<T>::remove_fixup(RbtNode<T>* current) {
    while (current && current != this->root && current->color == BLACK) {
      if (current == current->parent->left) {
        RbtNode<T>* sibling = current->parent->right;

        if (sibling->color == RED) {
          sibling->color = BLACK;
          current->parent->color = RED;
          this->left_rotate(current->parent);
          sibling = current->parent->right;
        }

        if (sibling->left->color == BLACK && sibling->right->color == BLACK) {
          sibling->color = RED;
          current = current->parent;
        }
        else {
          if (sibling->right->color == BLACK) {
            sibling->left->color = BLACK;
            sibling->color = RED;
            this->right_rotate(sibling);
            sibling = current->parent->right;
          }

          sibling->color = current->parent->color;
          current->parent->color = BLACK;
          sibling->right->color = BLACK;
          this->left_rotate(current->parent);
          current = this->root;
        }
      }
      else {
        RbtNode<T>* sibling = current->parent->left;

        if (sibling->color == RED) {
          sibling->color = BLACK;
          current->parent->color = RED;
          this->right_rotate(current->parent);
          sibling = current->parent->left;
        }

        if (sibling->right->color == BLACK && sibling->left->color == BLACK) {
          sibling->color = RED;
          current = current->parent;
        }
        else {
          if (sibling->left->color == BLACK) {
            sibling->right->color = BLACK;
            sibling->color = RED;
            this->left_rotate(sibling);
            sibling = current->parent->left;
          }

          sibling->color = current->parent->color;
          current->parent->color = BLACK;
          sibling->left->color = BLACK;
          this->right_rotate(current->parent);
          current = this->root;
        }
      }
    }

    if (current) current->color = BLACK;
    this->msize--;
  }

  template<typename T>
  RbtNode<T>* RbtTree<T>::lookup(const T key) const {
    if (this->empty()) return nullptr;
    RbtNode<T>* current = this->root;

    while (current) {
      if (key == current->key) return current;
      else if (key < current->key) current = current->left;
      else current = current->right;
    }

    return nullptr;
  }

  template<typename T>
  Vector::Vector<RbtNode<T>*> RbtTree<T>::preorder() const {
    Vector::Vector<RbtNode<T>*> path;
    if (this->empty()) return path;
    RbtNode<T>* current = this->root;
    Stack::StackArray<RbtNode<T>*>* s = new Stack::StackArray<RbtNode<T>*>(this->msize);
    s->push(current);

    while (!s->empty()) {
      current = s->top();
      s->pop();
      if (current == this->null) continue;
      path.push(current);
      if (current->right != this->null) s->push(current->right);
      if (current->left != this->null) s->push(current->left);
    }

    delete s;
    return path;
  }

  template<typename T>
  Vector::Vector<RbtNode<T>*> RbtTree<T>::inorder() const {
    Vector::Vector<RbtNode<T>*> path;
    if (this->empty()) return path;
    RbtNode<T>* current = this->root;
    Stack::StackArray<RbtNode<T>*>* s = new Stack::StackArray<RbtNode<T>*>(this->msize);
    
    while (!s->empty() || current != this->null) {
      if (current != this->null) {
        s->push(current);
        current = current->left;
      }
      else {
        current = s->top();
        s->pop();
        path.push(current);
        current = current->right;
      }
    }

    delete s;
    return path;
  }

  template<typename T>
  Vector::Vector<RbtNode<T>*> RbtTree<T>::postorder() const {
    Vector::Vector<RbtNode<T>*> path;
    if (this->empty()) return path;
    RbtNode<T>* current = this->root;
    Stack::StackArray<RbtNode<T>*>* s = new Stack::StackArray<RbtNode<T>*>(this->msize);
    Stack::StackArray<RbtNode<T>*>* t = new Stack::StackArray<RbtNode<T>*>(this->msize);
    s->push(current);

    while (!s->empty()) {
      current = s->top();
      s->pop();
      if (current == this->null) continue;
      t->push(current);
      if (current->left != this->null) s->push(current->left);
      if (current->right != this->null) s->push(current->right);
    }

    while (!t->empty()) {
      path.push(t->top());
      t->pop();
    }

    delete s;
    delete t;
    return path;
  }

  template<typename T>
  Vector::Vector<RbtNode<T>*> RbtTree<T>::levelorder() const {
    Vector::Vector<RbtNode<T>*> path;
    if (this->empty()) return path;
    RbtNode<T>* current = this->root;
    Queue::QueueArray<RbtNode<T>*>* q = new Queue::QueueArray<RbtNode<T>*>(this->msize);
    q->push(current);

    while (!q->empty()) {
      current = q->front();
      q->pop();
      if (current == this->null) continue;
      path.push(current);
      if (current->left != this->null) q->push(current->left);
      if (current->right != this->null) q->push(current->right);
    }

    delete q;
    return path;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const RbtTree<T>& tree) {
    Vector::Vector<RbtNode<T>*> path = tree.inorder();
    std::copy(path.begin(), path.end(), std::ostream_iterator<RbtNode<T>*>(out, " "));
    return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const RbtTree<T>* tree) {
    Vector::Vector<RbtNode<T>*> path = tree->inorder();
    std::copy(path.begin(), path.end(), std::ostream_iterator<RbtNode<T>*>(out, " "));
    return out;
  }
};

#endif

#ifndef RBT_NODE_H_INCLUDED
#define RBT_NODE_H_INCLUDED

#include <ostream>

enum COLOR { BLACK, RED };

namespace Tree {
  template<typename T>
  class RbtNode {
    public:
      T key;
      enum COLOR color;
      RbtNode<T>* left, *right, *parent;
      RbtNode(const T key, const enum COLOR color = RED, RbtNode<T>* left = nullptr, RbtNode<T>* right = nullptr, RbtNode<T>* parent = nullptr);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const RbtNode<__T>& node);
      template<typename __T> friend std::ostream& operator << (std::ostream& out, const RbtNode<__T>* node);
  };

  template<typename T>
  RbtNode<T>::RbtNode(const T key, const enum COLOR color, RbtNode<T>* left, RbtNode<T>* right, RbtNode<T>* parent) {
    this->key = key;
    this->color = color;
    this->left = left;
    this->right = right;
    this->parent = parent;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const RbtNode<T>& node) {
    out << "(" << node.key << ", " << (node.color ? "R" : "B") << ")"; return out;
  }

  template<typename T>
  std::ostream& operator << (std::ostream& out, const RbtNode<T>* node) {
    out << "(" << node->key << ", " << (node->color ? "R" : "B") << ")"; return out;
  }
}

#endif

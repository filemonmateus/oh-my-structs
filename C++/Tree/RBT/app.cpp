#include <iostream>
#include "lib/RbtTree.h"

// templated class shortener/alias
using Rbt = Tree::RbtTree<int>;

int main() {
  // init tree
  Rbt* tree = new Rbt;

  // consturct tree
  tree->insert(12);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(10);
  tree->insert(13);
  tree->insert(17);
  tree->insert(4);
  tree->insert(7);
  tree->insert(11);
  tree->insert(14);
  tree->insert(6);
  tree->insert(8);

  // inspect tree
  std::cout << tree->preorder() << std::endl;
  std::cout << tree->inorder() << std::endl;
  std::cout << tree->postorder() << std::endl;
  std::cout << tree->levelorder() << std::endl;
  std::cout << tree->height() << std::endl;

  // that's all
  delete tree;
}

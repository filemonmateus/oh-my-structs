public class Driver {
  public static void main(String[] args) {
    // init array container
    Array<Integer> array = new Array<Integer>();

    // append ints
    array.push(2);
    array.push(3);
    array.push(5);
    array.push(7);

    // insert ints
    array.insert(0,1);
    array.insert(3,4);
    array.insert(5,6);

    // inspect array
    System.out.println(array);
  }
}

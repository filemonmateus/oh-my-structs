@SuppressWarnings("unchecked")
public class Array<T extends Comparable<T>> implements Iterable<T> {
  private int size;
  private int capacity;
  private T[] elements;

  public Array() { this(10); }

  public Array(int capacity) {
    assert capacity > 0;
    this.size = 0;
    this.capacity = capacity;
    this.elements = (T[]) new Comparable[this.capacity];
  }

  public void push(T element) {
    if (this.size == this.capacity) this.resize();
    this.elements[this.size++] = element;
  }

  public void insert(int index, T element) {
    assert index < this.capacity;
    if (this.size == this.capacity) this.resize();
    for (int i = this.size; i > index; --i) {
      this.elements[i] = this.elements[i-1];
    }
    this.elements[index] = element;
    this.size++;
  }

  public void remove(int index) {
    assert index < this.size;
    for (int i = index; i < this.size - 1; ++i) {
      this.elements[i] = this.elements[i+1];
    }
    this.size--;
  }

  public void erase(T element) {
    for (int i = 0; i < this.size; ++i) {
      if (this.elements[i] == element) {
        this.remove(i);
      }
    }
  }

  public void swap(int index1, int index2) {
    T value1 = this.get(index1);
    T value2 = this.get(index2);
    this.set(index1, value2);
    this.set(index2, value1);
  }

  public void reverse() {
    for (int i = 0; i < this.size/2; ++i) {
      this.swap(i, this.size-i-1);
    }
  }

  private void resize() {
    this.capacity *= 2;
    T[] elements = (T[]) new Comparable[this.capacity];
    for (int i = 0; i < this.size; ++i) {
      elements[i] = this.elements[i];
    }
    this.elements = elements;
  }

  public void set(int index, T element) {
    assert index < this.capacity;
    this.elements[index] = element;
  }

  public T get(int index) {
    assert index < this.size;
    return this.elements[index];
  }

  public boolean full() {
    return this.size == this.capacity;
  }

  public boolean empty() {
    return this.size == 0;
  }

  public int size() {
    return this.size;
  }

  @Override
  public java.util.Iterator<T> iterator() {
    return new java.util.Iterator<T>() {
      int index = 0;
      public boolean hasNext() { return index < size; }
      public T next() { return elements[index++]; }
    };
  }

  @Override
  public String toString() {
    String repr = "";
    for (int i = 0; i < this.size; ++i) {
      repr += this.elements[i] + " ";
    }
    return repr;
  }
}

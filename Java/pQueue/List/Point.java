public class Point {
  private final double x;
  private final double y;
  private final double d;

  public Point (double x, double y) {
    this.x = x;
    this.y = y;
    this.d = this.x * this.x + this.y * this.y;
  }

  public double getX () { return this.x; }
  public double getY () { return this.y; }
  public double getD () { return this.d; }

  @Override public String toString () {
    return "(" + this.x + "," + this.y + ")";
  }
}

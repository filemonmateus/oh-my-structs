@SuppressWarnings("unchecked")
public class Entry<T> implements Comparable<Entry<T>> {
  protected final T key;
  protected final Double priority;

  public Entry (T key, Double priority) {
    this.key = key;
    this.priority = priority;
  }

  public T getKey () {
    return this.key;
  }

  public Double getPriority () {
    return this.priority;
  }

  public int compareTo (Entry<T> that) {
    return (double) this.priority < (double) that.priority ? -1 : (double) this.priority > (double) that.priority ? 1 : 0;
  }

  @Override
  public String toString () {
    return "{" + this.key + ", " + this.priority + "}";
  }
}

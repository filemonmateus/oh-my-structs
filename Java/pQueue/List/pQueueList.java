@SuppressWarnings("unchecked")
public class pQueueList<T> extends SinglyLinkedList<Entry<T>> {
  public pQueueList () {}

  public void push (T key, Double priority) {
    if (this.empty() || priority > this.head.key.priority) {
      this.prepend(new Entry<T>(key, priority));
      return;
    }

    Node<Entry<T>> curr = this.head.next;
    Node<Entry<T>> pred = this.head;

    while (curr != null) {
      if (curr.key.priority > priority) {
        pred = curr;
        curr = curr.next;
      }
      else {
        Node<Entry<T>> node = new Node<Entry<T>>(new Entry<T>(key, priority));
        pred.next = node;
        node.next = curr;
        this.size++;
        return;
      }
    }

    this.append(new Entry<T>(key, priority));
  }

  public void pop () {
    this.remove(this.head.key);
  }

  public Entry<T> top () {
    return this.head.key;
  }

  @Override
  public String toString () {
    String repr = "[";
    for (Node<Entry<T>> curr = this.head; curr != null; curr = curr.next) {
      repr += "{" + curr.key.key + ":" + curr.key.priority + "}->";
    }
    repr += "null";
    return repr;
  }
}

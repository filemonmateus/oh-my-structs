public class Driver {
  public static Array<Point> kclosests(Array<Point> points, int k) {
    assert k > 0;
    if (points.size() <= k) return points;
    pQueueHeap<Point> maxheap = new pQueueHeap<Point>();
    Array<Point> closests = new Array<Point>();

    for (int i = 0; i < k; ++i) {
      maxheap.push(points.get(i), points.get(i).getD());
    }

    for (int i = k; i < points.size(); ++i) {
      if (points.get(i).getD() < maxheap.top().getPriority()) {
        maxheap.pop();
        maxheap.push(points.get(i), points.get(i).getD());
      }
    }

    while (!maxheap.empty()) {
      closests.push(maxheap.top().getKey());
      maxheap.pop();
    }

    return closests;
  }

  public static void main(String[] args) {
    Array<Point> points = new Array<Point>();
    points.push(new Point(4,4));
    points.push(new Point(1,1));
    points.push(new Point(6,6));
    points.push(new Point(2,2));
    points.push(new Point(0,0));
    points.push(new Point(3,3));
    points.push(new Point(5,5));
    System.out.println(points);
    Array<Point> closests = kclosests(points, 2);
    System.out.println(closests);
  }
}

@SuppressWarnings("unchecked")
public class pQueueHeap<T> extends MaxHeap<Entry<T>> {
  public pQueueHeap() { super(); }

  public pQueueHeap(int capacity) { super(capacity); }

  public void push(T key, Double priority) {
    super.push(new Entry<T>(key, priority));
  }

  public void pop() {
    super.pop();
  }

  public Entry<T> top() {
    return super.top();
  }
}

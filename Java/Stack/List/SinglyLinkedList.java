@SuppressWarnings("unchecked")
public class SinglyLinkedList<T> {
  protected Node<T> head;
  protected int size;

  public SinglyLinkedList () {
    this.head = null;
    this.size = 0;
  }

  public void prepend (T key) {
    Node<T> node = new Node<T>(key);
    node.next = this.head;
    this.head = node;
    this.size++;
  }

  public void append (T key) {
    if (this.empty()) {
      this.prepend(key);
      return;
    }

    Node<T> curr = this.head.next;
    Node<T> pred = this.head;

    while (curr != null) {
      pred = curr;
      curr = curr.next;
    }

    pred.next = curr = new Node<T>(key);
    this.size++;
  }

  public void remove (T key) {
    if (this.empty()) {
      return;
    }
    else if (this.head.key == key) {
      Node<T> trash = this.head;
      this.head = this.head.next;
      trash = null;
      this.size--;
      return;
    }

    Node<T> trash = this.head.next;
    Node<T> pred = this.head;

    while (trash != null) {
      if (trash.key == key) {
        pred.next = trash.next;
        trash = null;
        this.size--;
        return;
      }
      pred = trash;
      trash = trash.next;
    }
  }

  public boolean lookup (T key) {
    for (Node<T> curr = this.head; curr != null; curr = curr.next) {
      if (curr.key == key) {
        return true;
      }
    }
    return false;
  }

  public boolean empty () {
    return this.size == 0;
  }

  public int size () {
    return this.size;
  }

  public void reverse () {
    if (this.size < 2) return;
    Node<T> curr = this.head;
    Node<T> successor = null;
    Node<T> pred = null;

    while (curr != null) {
      successor = curr.next;
      curr.next = pred;
      pred = curr;
      curr = successor;
    }

    this.head = pred;
  }

  @Override
  public String toString () {
    String repr = "";
    for (Node<T> curr = this.head; curr != null; curr = curr.next) {
      repr += curr.key + "->";
    }
    repr += "null";
    return repr;
  }
}

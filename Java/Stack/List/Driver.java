public class Driver {
  public static void main (String[] args) {
    // init stack
    Stack<Integer> s = new Stack<Integer>();

    // push keys
    s.push(1);
    s.push(2);
    s.push(3);
    s.push(4);
    s.push(5);

    // inspect them
    System.out.println(s);

    // inspect LIFO invariance
    System.out.println("top() -> " + s.top());
    s.pop();
    System.out.println("top() -> " + s.top());
    s.pop();

    // inspect s state
    System.out.println(s);
  }
}

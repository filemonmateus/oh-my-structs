@SuppressWarnings("unchecked")
public class Stack<T> extends SinglyLinkedList<T> {
  public Stack () {}

  public void push (T key) {
    this.prepend(key);
  }

  public void pop () {
    assert !this.empty();
    this.remove(this.head.key);
  }

  public T top () {
    assert !this.empty();
    return this.head.key;
  }

  @Override public String toString () {
    String repr = "";
    for (Node<T> current = this.head; current != null; current = current.next) {
      repr += current.key + " "; 
    }
    return repr;
  }
}

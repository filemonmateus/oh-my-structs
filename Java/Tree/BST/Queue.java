@SuppressWarnings("unchecked")
public class Queue<T> {
  private int front;
  private int back;
  private int capacity;
  private T[] elements;

  public Queue () { this(10); }

  public Queue (int capacity) {
    this.front = this.back = 0;
    this.capacity = capacity;
    this.elements = (T[]) new Object[this.capacity];
  }

  public boolean full () {
    return this.size() == this.capacity;
  }

  public boolean empty () {
    return this.size() == 0;
  }

  public int size () {
    return this.back - this.front;
  }

  public void resize () {
    this.capacity *= 2;
    T[] elements = (T[]) new Object[this.capacity];
    for (int i = 0; i < this.size(); ++i) {
      elements[i] = this.elements[this.front+i];
    }
    this.elements = elements;
  }

  public void push (T element) {
    if (this.full()) this.resize();
    this.elements[this.back++] = element;
  }

  public T front () {
    assert !this.empty();
    return this.elements[this.front];
  }

  public void pop () {
    assert !this.empty();
    this.front++;
  }

  @Override
  public String toString () {
    String repr = "";
    for (int i = this.front; i < this.back; ++i) {
      repr += this.elements[i] + " ";
    }
    return repr;
  }

}

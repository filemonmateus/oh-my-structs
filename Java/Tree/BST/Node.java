@SuppressWarnings("unchecked")
public class Node<T> {
  public T key;
  public Node<T> left;
  public Node<T> right;

  public Node (T key) {
    this.key = key;
    this.left = this.right = null;
  }

  @Override
  public String toString () {
    String repr = "";
    repr += this.key;
    return repr;
  }
}

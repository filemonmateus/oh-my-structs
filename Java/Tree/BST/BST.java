@SuppressWarnings("unchecked")
public class BST<T extends Comparable<T>> {
  private Node<T> root;
  private int size;

  public BST () {
    this.root = null;
    this.size = 0;
  }

  public void insert (T key) {
    if (this.empty()) {
      this.root = new Node<T>(key);
      this.size++;
      return;
    }

    Node<T> curr = this.root;

    while (curr != null) {
      if (curr.key.compareTo(key) == 1) {
        if (curr.left != null) {
          curr = curr.left;
        }
        else {
          curr.left = new Node<T>(key);
          this.size++;
          return;
        }
      }
      else {
        if (curr.right != null) {
          curr = curr.right;
        }
        else {
          curr.right = new Node<T>(key);
          this.size++;
          return;
        }
      }
    }
  }

  private Node<T> _get_max_left_subtree_node (Node<T> curr) {
    return curr.right == null ? curr : this._get_max_left_subtree_node(curr.right);
  }

  private Node<T> _get_min_right_subtree_node (Node<T> curr) {
    return curr.left == null ? curr : _get_min_right_subtree_node(curr.left);
  }

  private Node<T> remove (Node<T> curr, T key) {
    if (curr == null) return curr;
    else if (key.compareTo(curr.key) == -1) curr.left = this.remove(curr.left, key);
    else if (key.compareTo(curr.key) == +1) curr.right = this.remove(curr.right, key);
    else {
      // case 0 children
      if (curr.left == null && curr.right == null) {
        curr = null;
      }
      // case 1 child
      else if (curr.left == null) {
        Node<T> trash = curr;
        curr = curr.right;
        trash = null;
      }
      else if (curr.right == null) {
        Node<T> trash = curr;
        curr = curr.left;
        trash = null;
      }
      // case 2 children
      else {
        Node<T> trash = this._get_max_left_subtree_node(curr.left);
        curr.key = trash.key;
        curr.left = this.remove(curr.left, curr.key);
      }
    }

    return curr;
  }

  public void remove (T key) {
    this.root = this.remove(this.root, key);
    this.size--;
  }

  public boolean lookup (T key) {
    if (this.empty()) return false;
    Node<T> curr = this.root;

    while (curr != null) {
      if (curr.key.compareTo(key) == 0) return true;
      else if (curr.key.compareTo(key) == 1) curr = curr.left;
      else curr = curr.right;
    }

    return false;
  }

  public boolean empty () {
    return this.size == 0;
  }

  public int size () {
    return this.size;
  }

  private int height (Node<T> curr) {
    return curr != null ? Math.max(this.height(curr.left), this.height(curr.right)) + 1 : 0;
  }

  public int height () {
    return this.height(this.root);
  }

  public T min () {
    assert !this.empty();
    Node<T> curr = this.root;
    while (curr.left != null) {
      curr = curr.left;
    }
    return curr.key;
  }

  public T max () {
    assert !this.empty();
    Node<T> curr = this.root;
    while (curr.right != null) {
      curr = curr.right;
    }
    return curr.key;
  }

  public Array<Node<T>> preorder () {
    Array<Node<T>> path = new Array<Node<T>>(this.size);
    if (this.empty()) return path;
    Stack<Node<T>> s = new Stack<Node<T>>(this.size);
    Node<T> curr = this.root;
    s.push(curr);

    while (!s.empty()) {
      curr = s.top();
      s.pop();
      path.push(curr);
      if (curr.right != null) s.push(curr.right);
      if (curr.left != null) s.push(curr.left);
    }

    return path;
  }

  public Array<Node<T>> inorder () {
    Array<Node<T>> path = new Array<Node<T>>(this.size);
    if (this.empty()) return path;
    Stack<Node<T>> s = new Stack<Node<T>>(this.size);
    Node<T> curr = this.root;

    while (!s.empty() || curr != null) {
      if (curr != null) {
        s.push(curr);
        curr = curr.left;
      }
      else {
        curr = s.top();
        s.pop();
        path.push(curr);
        curr = curr.right;
      }
    }

    return path;
  }

  public Array<Node<T>> postorder () {
    Array<Node<T>> path = new Array<Node<T>>(this.size);
    if (this.empty()) return path;
    Stack<Node<T>> s = new Stack<Node<T>>(this.size);
    Stack<Node<T>> t = new Stack<Node<T>>(this.size);
    Node<T> curr = this.root;
    s.push(curr);

    while (!s.empty()) {
      curr = s.top();
      s.pop();
      t.push(curr);
      if (curr.left != null) s.push(curr.left);
      if (curr.right != null) s.push(curr.right);
    } 

    while (!t.empty()) {
      path.push(t.top());
      t.pop();
    }

    return path;
  }

  public Array<Node<T>> levelorder () {
    Array<Node<T>> path = new Array<Node<T>>(this.size);
    if (this.empty()) return path;
    Queue<Node<T>> q = new Queue<Node<T>>(this.size);
    Node<T> curr = this.root;
    q.push(curr);

    while (!q.empty()) {
      curr = q.front();
      q.pop();
      path.push(curr);
      if (curr.left != null) q.push(curr.left);
      if (curr.right != null) q.push(curr.right);
    }

    return path;
  }

  @Override
  public String toString () {
    Array<Node<T>> path = this.inorder();
    return path.toString();
  }
}

public class Driver {
  public static void main (String[] args) {
    // init tree
    BST<Integer> tree = new BST<Integer>();

    // construct tree
    tree.insert(10);
    tree.insert(06);
    tree.insert(14);
    tree.insert(04);
    tree.insert(07);
    tree.insert(12);
    tree.insert(16);

    // inspect tree
    System.out.println(tree.preorder());
    System.out.println(tree.inorder());
    System.out.println(tree.postorder());
    System.out.println(tree.levelorder());
  }
}

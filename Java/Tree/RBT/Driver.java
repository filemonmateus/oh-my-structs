public class Driver {
  public static void main (String[] args) {
    // init tree
    RBT<Integer> tree = new RBT<Integer>();

    // construct tree
    tree.insert(12);
    tree.insert(5);
    tree.insert(15);
    tree.insert(3);
    tree.insert(10);
    tree.insert(13);
    tree.insert(17);
    tree.insert(4);
    tree.insert(7);
    tree.insert(11);
    tree.insert(14);
    tree.insert(6);
    tree.insert(8);

    // inspect tree
    System.out.println(tree.preOrder());
    System.out.println(tree.inOrder());
    System.out.println(tree.postOrder());
    System.out.println(tree.levelOrder());
    System.out.println(tree.height());
  }
}

@SuppressWarnings("unchecked")
public class RBT<T extends Comparable<T>> {
  private int size;
  private Node<T> nill;
  private Node<T> root;

  public RBT () {
    this.size = 0;
    this.nill = new Node<T>(null, false);
    this.root = this.nill;
  }

  public boolean empty () {
    return this.size == 0;
  }

  public int height () {
    return this.height(this.root);
  }

  private int height (Node<T> current) {
    return current == this.nill ? 0 : Math.max(this.height(current.left), this.height(current.right)) + 1;
  }

  private Node<T> minimum (Node<T> current) {
    return current.left != null ? this.minimum(current.left) : current;
  }

  private Node<T> maximum (Node<T> current) {
    return current.right != null ? this.maximum(current.right) : current;
  }

  private Node<T> successor (Node<T> current) {
    if (current.right != null) return this.minimum(current.right);
    Node<T> parent = current.parent;
    while (parent != null && current == parent.right) {
      current = parent;
      parent = parent.parent;
    }
    return parent;
  }

  private Node<T> predecessor (Node<T> current) {
    if (current.left != null) return this.maximum(current.left);
    Node<T> parent = current.parent;
    while (parent != null && current == parent.left) {
      current = parent;
      parent = parent.parent;
    }
    return parent;
  }

  private void transplant (Node<T> thisNode, Node<T> thatNode) {
    if (thisNode.parent == this.nill) {
      this.root = thatNode;
    }
    else if (thisNode == thisNode.parent.left) {
      thisNode.parent.left = thatNode;
    }
    else {
      thisNode.parent.right = thatNode;
    }
    thatNode.parent = thisNode.parent;
  }

  private void leftRotate (Node<T> current) {
    Node<T> child = current.right;
    current.right = child.left;

    if (child.left != this.nill) {
      child.left.parent = current;
    }

    child.parent = current.parent;

    if (current.parent == this.nill) {
      this.root = child;
    }
    else if (current == current.parent.left) {
      current.parent.left = child;
    }
    else {
      current.parent.right = child;
    }

    child.left = current;
    current.parent = child;
  }

  private void rightRotate (Node<T> current) {
    Node<T> child = current.left;
    current.left = child.right;

    if (child.right != this.nill) {
      child.right.parent = current;
    }

    child.parent = current.parent;

    if (current.parent == this.nill) {
      this.root = child;
    }
    else if (current == current.parent.right) {
      current.parent.right = child;
    }
    else {
      current.parent.left = child;
    }

    child.right = current;
    current.parent = child;
  }

  public void insert (T key) {
    Node<T> parent = this.nill;
    Node<T> current = this.root;
    Node<T> newnode = new Node(key, true);

    while (current != this.nill) {
      parent = current;

      if (newnode.key.compareTo(current.key) == -1) {
        current = current.left;
      }
      else {
        current = current.right;
      }
    }

    newnode.parent = parent;

    if (parent == this.nill) {
      this.root = newnode;
    }
    else if (newnode.key.compareTo(parent.key) == -1) {
      parent.left = newnode;
    }
    else {
      parent.right = newnode;
    }

    newnode.left = this.nill;
    newnode.right = this.nill;
    newnode.color = true;
    this.insertFixup(newnode);
  }

  private void insertFixup (Node<T> current) {
    while (current.parent.color == true) {
      if (current.parent == current.parent.parent.left) {
        Node<T> uncle = current.parent.parent.right;

        if (uncle.color == true) {
          current.parent.color = false;
          uncle.color = false;
          current.parent.parent.color = true;
          current = current.parent.parent;
        }
        else {
          if (current == current.parent.right) {
            current = current.parent;
            this.leftRotate(current);
          }
          
          current.parent.color = false;
          current.parent.parent.color =  true;
          this.rightRotate(current.parent.parent);
        }
      }
      else {
        Node<T> uncle = current.parent.parent.left;

        if (uncle.color == true) {
          current.parent.color = false;
          uncle.color = false;
          current.parent.parent.color = true;
          current = current.parent.parent;
        }
        else {
          if (current == current.parent.left) {
            current = current.parent;
            this.rightRotate(current);
          }

          current.parent.color = false;
          current.parent.parent.color = true;
          this.leftRotate(current.parent.parent);
        }
      }
    }

    this.root.color = false;
    this.size++;
  }

  public void remove (T key) {
    Node<T> trash = this.lookup(key);
    if (trash == null) return;
    Node<T> trash_copy = trash;
    Node<T> child;
    Boolean trash_copy_original_color = trash_copy.color;
    
    if (trash.left == this.nill) {
      child = trash.right;
      this.transplant(trash, trash.right);
    }
    else if (trash.right == this.nill) {
      child = trash.left;
      this.transplant(trash, trash.left);
    }
    else {
      trash_copy = this.minimum(trash.right);
      trash_copy_original_color = trash_copy.color;
      child = trash_copy.right;

      if (trash_copy.parent == trash) {
        child.parent = trash;
      }
      else {
        this.transplant(trash_copy, trash_copy.right);
        trash_copy.right = trash.right;
        trash_copy.right.parent = trash_copy;
      }

      this.transplant(trash, trash_copy);
      trash_copy.left = trash.left;
      trash_copy.left.parent = trash_copy;
      trash_copy.color = trash.color;
    }

    if (trash_copy_original_color == false) {
      this.removeFixup(child);
    }
  }

  private void removeFixup (Node<T> current) {
    while (current != null && current != this.root && current.color == false) {
      if (current == current.parent.left) {
        Node<T> sibling = current.parent.right;

        if (sibling.color == true) {
          sibling.color = false;
          current.parent.color = true;
          this.leftRotate(current.parent);
          sibling = current.parent.right;
        }

        if (sibling.left.color == false && sibling.right.color == false) {
          sibling.color = true;
          current = current.parent;
        }
        else {
          if (sibling.right.color == false) {
            sibling.left.color = false;
            sibling.color = true;
            this.rightRotate(sibling);
            sibling = current.parent.right;
          }

          sibling.color = current.parent.color;
          current.parent.color = false;
          sibling.right.color = false;
          this.leftRotate(current.parent);
          current = this.root;
        }
      }
      else {
        Node<T> sibling = current.parent.left;

        if (sibling.color == true) {
          sibling.color = false;
          current.parent.color = true;
          this.rightRotate(current.parent);
          sibling = current.parent.left;
        }

        if (sibling.right.color == false && sibling.left.color == false) {
          sibling.color = true;
          current = current.parent;
        }
        else {
          if (sibling.left.color == false) {
            sibling.right.color = false;
            sibling.color = true;
            this.leftRotate(sibling);
            sibling = current.parent.left;
          }

          sibling.color = current.parent.color;
          current.parent.color = false;
          sibling.left.color = false;
          this.rightRotate(current.parent);
          current = this.root;
        }
      }
    }

    if (current != null) current.color = false;
    this.size--;
  }

  public Node<T> lookup (T key) {
    if (this.empty()) return null;
    Node<T> current = this.root;

    while (current != null) {
      if (key.compareTo(current.key) == -1) current = current.left;
      else if (key.compareTo(current.key) == +1)  current = current.right;
      else return current;
    }

    return null;
  }

  public Array<Node<T>> preOrder () {
    Array<Node<T>> path = new Array<Node<T>>(this.size);
    if (this.empty()) return path;
    Node<T> current = this.root;
    Stack<Node<T>> s = new Stack<Node<T>>(this.size);
    s.push(current);

    while (!s.empty()) {
      current = s.top();
      s.pop();
      if (current == this.nill) continue;
      path.push(current);
      if (current.right != null && current.right != this.nill) s.push(current.right);
      if (current.left != null && current.left != this.nill) s.push(current.left);
    }

    return path;
  }

  public Array<Node<T>> inOrder () {
    Array<Node<T>> path = new Array<Node<T>>(this.size);
    if (this.empty()) return path;
    Node<T> current = this.root;
    Stack<Node<T>> s = new Stack<Node<T>>(this.size);

    while (!s.empty() || current != this.nill) {
      if (current != this.nill) {
        s.push(current);
        current = current.left;
      }
      else {
        current = s.top();
        s.pop();
        path.push(current);
        current = current.right;
      }
    }

    return path;
  }

  public Array<Node<T>> postOrder () {
    Array<Node<T>> path = new Array<Node<T>>(this.size);
    if (this.empty()) return path;
    Stack<Node<T>> s = new Stack<Node<T>>(this.size);
    Stack<Node<T>> t = new Stack<Node<T>>(this.size);
    Node<T> current = this.root;
    s.push(current);

    while (!s.empty()) {
      current = s.top();
      s.pop();
      if (current == this.nill) continue;
      t.push(current);
      if (current.left != null && current.left != this.nill) s.push(current.left);
      if (current.right != null && current.right != this.nill) s.push(current.right);
    } 

    while (!t.empty()) {
      path.push(t.top());
      t.pop();
    }

    return path;
  }

  public Array<Node<T>> levelOrder () {
    Array<Node<T>> path = new Array<Node<T>>(this.size);
    if (this.empty()) return path;
    Queue<Node<T>> q = new Queue<Node<T>>(this.size);
    Node<T> current = this.root;
    q.push(current);

    while (!q.empty()) {
      current = q.front();
      q.pop();
      if (current == this.nill) continue;
      path.push(current);
      if (current.left != null && current.left != this.nill) q.push(current.left);
      if (current.right != null && current.right != this.nill) q.push(current.right);
    }

    return path;
  }

  @Override
  public String toString () {
    Array<Node<T>> path = this.inOrder(); 
    return path.toString();
  }
}

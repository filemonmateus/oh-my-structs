public class Node<T> {
  public T key;
  public Boolean color;
  Node<T> left;
  Node<T> right;
  Node<T> parent;

  public Node (T key, Boolean color) {
    this(key, color, null, null, null);
  }

  public Node (T key, Boolean color, Node<T> left, Node<T> right, Node<T> parent) {
    this.key = key;
    this.color = color;
    this.left = left;
    this.right = right;
    this.parent = parent;
  }

  private String getColor () {
    return this.color ? "R" : "B";
  }

  @Override
  public String toString () {
    String repr = "";
    repr += "(" + this.key + ", " + this.getColor() + ")"; 
    return repr;
  }
}

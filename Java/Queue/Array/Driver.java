public class Driver {
  public static void main(String[] args) {
    // init queue
    Queue<Integer> q = new Queue<Integer>();

    // add values
    q.push(1);
    q.push(2);
    q.push(3);
    q.push(4);
    q.push(5);

    // inspect queue
    System.out.println(q.toString());

    // check FIFO invariance
    System.out.println("front() -> " + q.front());
    q.pop();
    System.out.println("front() -> " + q.front());
    q.pop();

    // check queue state
    System.out.println(q);
  }
}

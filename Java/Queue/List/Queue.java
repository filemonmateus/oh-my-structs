@SuppressWarnings("unchecked")
public class Queue<T> extends DoublyLinkedList<T> {
  public Queue() {}

  public void push(T element) {
    this.append(element);
  }

  public void pop() {
    assert !this.empty();
    this.remove(this.head.key);
  }

  public T front() {
    assert !this.empty();
    return (this.head.key);
  }

  @Override public String toString() {
    String repr = "";
    for (Node<T> curr = this.head; curr != null; curr = curr.next) {
      repr += curr.key + " ";
    }
    return repr;
  }
}

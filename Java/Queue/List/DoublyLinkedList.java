@SuppressWarnings("unchecked")
public class DoublyLinkedList<T> {
  protected Node<T> head;
  protected Node<T> tail;
  protected int size;

  public DoublyLinkedList() {
    this.size = 0;
    this.head = this.tail = null;
  }

  public void prepend(T key) {
    if (this.empty()) {
      this.head = this.tail = new Node<T>(key);
      this.size++;
      return;
    }
    Node<T> node = new Node<T>(key);
    node.next = this.head;
    this.head.prev = node;
    this.head = node;
    this.size++;
  }

  public void append(T key) {
    if (this.empty()) {
      this.prepend(key);
      return;
    }
    Node<T> node = new Node<T>(key);
    node.prev = this.tail;
    this.tail.next = node;
    this.tail = node;
    this.size++;
  }

  public void remove(T key) {
    if (this.empty()) {
      return;
    }
    else if (this.head.key == key) {
      Node<T> trash = this.head;
      this.head = this.head.next;

      if (this.head != null) {
        this.head.prev = trash.prev;
      }

      trash = null;
      this.size--;
      return;
    }
    else if (this.tail.key == key) {
      Node<T> trash = this.tail;
      this.tail = this.tail.prev;
      this.tail.next = trash.next;
      trash = null;
      this.size--;
      return;
    }

    Node<T> trash = this.head.next;

    while (trash != null) {
      if (trash.key == key) {
        trash.prev.next = trash.next;
        trash.next.prev = trash.prev;
        trash = null;
        this.size--;
        return;
      }
      trash = trash.next;
    }
  }

  public boolean contains(T key) {
    for (Node<T> curr = this.head; curr != null; curr = curr.next) {
      if (curr.key == key) {
        return true;
      }
    }
    return false;
  }

  public boolean empty() {
    return this.size == 0;
  }

  public int size() {
    return this.size;
  }

  public Node<T> head() {
    return this.head;
  }

  public Node<T> tail() {
    return this.tail;
  }

  public void reverse() {
    if (this.size < 2) return;
    Node<T> curr = this.head;
    Node<T> pred = null;

    while (curr != null) {
      pred = curr.prev;
      curr.prev = curr.next;
      curr.next = pred;
      curr = curr.prev;
    }

    this.head = this.tail;
    this.tail = pred.prev;
  }

  @Override
  public String toString() {
    String repr = "";

    for (Node<T> curr = this.head; curr != null; curr = curr.next) {
      repr += curr.key + "<->";
    }

    repr += "null";
    return repr;
  }
}

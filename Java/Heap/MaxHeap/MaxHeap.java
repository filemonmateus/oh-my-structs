@SuppressWarnings("unchecked")
public class MaxHeap<T extends Comparable<T>> extends Array<T> {

  public MaxHeap() { super(); }

  public MaxHeap(int capacity) { super(capacity); }

  private void heapify(int idx) {
    int max = idx, left = 2*idx, right = 2*idx+1;
    if (left < this.size() && this.elements[left].compareTo(this.elements[max]) == 1) max = left;
    if (right < this.size() && this.elements[right].compareTo(this.elements[max]) == 1) max = right;
    if (max != idx) { super.swap(max, idx); this.heapify(max); }
  }

  private void maxheap() {
    for (int i = this.size()/2; i >= 0; --i) this.heapify(i);
  }

  public void push(T element) {
    super.push(element);
    if (element.compareTo(this.top()) == 1) this.maxheap();
  }

  public void pop() {
    super.remove(0);
    this.maxheap();
  } 

  public T top() {
    return super.get(0);
  }
}

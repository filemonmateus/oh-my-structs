public class Driver {
  public static void main(String[] args) {
    MaxHeap<Integer> maxheap = new MaxHeap<Integer>(5);
    maxheap.push(4);
    maxheap.push(3);
    maxheap.push(7);
    maxheap.push(9);
    maxheap.push(1);
    System.out.println(maxheap);
  }
}

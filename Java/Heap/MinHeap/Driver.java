public class Driver {
  public static void main(String[] args) {
    MinHeap<Integer> minheap = new MinHeap<Integer>(5);
    minheap.push(4);
    minheap.push(3);
    minheap.push(7);
    minheap.push(9);
    minheap.push(1);
    System.out.println(minheap);
  }
}

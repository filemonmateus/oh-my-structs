@SuppressWarnings("unchecked")
public class Edge<T> {
  public Vertex<T> src;
  public Vertex<T> dst;
  public Double weight;

  public Edge(Vertex<T> src, Vertex<T> dst) {
    this(src, dst, 0.0);
  }

  public Edge(Vertex<T> src, Vertex<T> dst, Double weight) {
    this.src = src;
    this.dst = dst;
    this.weight = weight;
    this.src.neighbors.add(this.dst);
  }

  @Override
  public String toString() {
    String repr = "";
    repr += src.key + "->" + dst.key + " (" + this.weight + ")";
    return repr;
  }

  @Override
  public int hashCode() {
    return this.src.hashCode() ^ this.dst.hashCode() ^ this.weight.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Edge)) return false;
    Edge<T> edge = (Edge<T>) other;
    return this.src.equals(edge.src) && this.dst.equals(edge.dst) && this.weight == edge.weight;
  }

  @Override
  protected void finalize() {
    this.src.neighbors.remove(this.dst);
  }
}

import java.util.Set;
import java.util.LinkedHashSet;

public class Graph<T> {
  public Set<Vertex<T>> vertices;
  public Set<Edge<T>> edges;

  public Graph() {
    this.vertices = new LinkedHashSet<Vertex<T>>();
    this.edges = new LinkedHashSet<Edge<T>>();
  }

  public void insertVertex(T key) {
    if (this.containsVertex(key)) return;
    this.vertices.add(new Vertex<T>(key));
  }

  public void removeVertex(T key) {
    Vertex<T> vertex = this.getVertex(key);
    if (vertex != null) {
      this.edges.removeIf((Edge<T> edge) -> { return edge.src == vertex || edge.dst == vertex; });
      this.vertices.remove(vertex);
    }
  }

  public void insertEdge(T src, T dst) {
    if (!this.containsVertex(src) || !this.containsVertex(dst)) return;
    if (this.containsEdge(src, dst)) return;
    this.edges.add(new Edge<T>(this.getVertex(src), this.getVertex(dst)));
  }

  public void insertEdge(T src, T dst, Double weight) {
    if (!this.containsVertex(src) || !this.containsVertex(dst)) return;
    if (this.containsEdge(src, dst, weight)) return;
    this.edges.add(new Edge<T>(this.getVertex(src), this.getVertex(dst), weight));
  }

  public void removeEdge(T src, T dst) {
    Edge<T> edge = this.getEdge(src, dst);
    if (edge != null) {
      this.edges.remove(edge);
      edge.finalize();
    }
  }

  public void removeEdge(T src, T dst, Double weight) {
    Edge<T> edge = this.getEdge(src, dst, weight);
    if (edge != null) {
      this.edges.remove(edge);
      edge.finalize();
    }
  }

  public boolean containsVertex(T key) {
    return this.getVertex(key) != null;
  }

  public boolean containsEdge(T src, T dst) {
    return this.getEdge(src, dst) != null;
  }

  public boolean containsEdge(T src, T dst, Double weight) {
    return this.getEdge(src, dst, weight) != null;
  }

  public Vertex<T> getVertex(T key) {
    for (Vertex<T> vertex : this.vertices) {
      if (vertex.equals(key)) {
        return vertex;
      }
    }
    return null;
  }

  public Edge<T> getEdge(T src, T dst) {
    for (Edge<T> edge : this.edges) {
      if (edge.src.equals(src) && edge.dst.equals(dst)) {
        return edge;
      }
    }
    return null;
  }

  public Edge<T> getEdge(T src, T dst, Double weight) {
    for (Edge<T> edge : this.edges) {
      if (edge.src.equals(src) && edge.dst.equals(dst) && edge.weight == weight) {
        return edge;
      }
    }
    return null;
  }

  public void markAllVerticesUnvisited() {
    for (Vertex<T> vertex : this.vertices) {
      vertex.prev = null;
      vertex.cost = Double.MAX_VALUE;
      vertex.visited = false;
    }
  }

  public Array<Vertex<T>> dfs(T src) {
    Vertex<T> srcVertex = this.getVertex(src);
    assert srcVertex != null;

    this.markAllVerticesUnvisited();
    Array<Vertex<T>> path = new Array<Vertex<T>>(this.vertices.size());
    Stack<Vertex<T>> visited = new Stack<Vertex<T>>(this.vertices.size());

    srcVertex.cost = 0.0;
    srcVertex.visited = true;
    visited.push(srcVertex);

    while (!visited.empty()) {
      Vertex<T> vertex = visited.top();
      visited.pop();
      path.push(vertex);

      for (Vertex<T> neighbor : vertex.neighbors) {
        if (!neighbor.visited) {
          neighbor.cost = vertex.cost + this.getEdge(vertex.key, neighbor.key).weight;
          neighbor.prev = vertex;
          neighbor.visited = true;
          visited.push(neighbor);
        }
      }
    }
    
    return path;
  }

  public Array<Vertex<T>> dfs(T src, T dst) {
    Vertex<T> srcVertex = this.getVertex(src);
    Vertex<T> dstVertex = this.getVertex(dst);
    assert srcVertex != null && dstVertex != null;

    this.markAllVerticesUnvisited();
    Array<Vertex<T>> path = new Array<Vertex<T>>(this.vertices.size());
    Stack<Vertex<T>> visited = new Stack<Vertex<T>>(this.vertices.size());

    srcVertex.cost = 0.0;
    srcVertex.visited = true;
    visited.push(srcVertex);

    while (!visited.empty()) {
      Vertex<T> vertex = visited.top();
      visited.pop();

      if (vertex.equals(dstVertex)) {
        for (Vertex<T> current = vertex; current != srcVertex.prev; current = current.prev) {
          path.push(current);
        }
        path.reverse();
        break;
      }

      for (Vertex<T> neighbor : vertex.neighbors) {
        if (!neighbor.visited) {
          neighbor.cost = vertex.cost + this.getEdge(vertex.key, neighbor.key).weight;
          neighbor.prev = vertex;
          neighbor.visited = true;
          visited.push(neighbor);
        }
      } 
    }

    return path;
  }

  public Array<Vertex<T>> bfs(T src) {
    Vertex<T> srcVertex = this.getVertex(src);
    assert srcVertex != null;

    this.markAllVerticesUnvisited();
    Array<Vertex<T>> path = new Array<Vertex<T>>(this.vertices.size());
    Queue<Vertex<T>> visited = new Queue<Vertex<T>>(this.vertices.size());

    srcVertex.cost = 0.0;
    srcVertex.visited = true;
    visited.push(srcVertex);

    while (!visited.empty()) {
      Vertex<T> vertex = visited.front();
      visited.pop();
      path.push(vertex);

      for (Vertex<T> neighbor : vertex.neighbors) {
        if (!neighbor.visited) {
          neighbor.cost = vertex.cost + this.getEdge(vertex.key, neighbor.key).weight;
          neighbor.prev = vertex;
          neighbor.visited = true;
          visited.push(neighbor);
        }
      }
    }

    return path;
  }

  public Array<Vertex<T>> bfs(T src, T dst) {
    Vertex<T> srcVertex = this.getVertex(src);
    Vertex<T> dstVertex = this.getVertex(dst);
    assert srcVertex != null && dstVertex != null;

    this.markAllVerticesUnvisited();
    Array<Vertex<T>> path = new Array<Vertex<T>>(this.vertices.size());
    Queue<Vertex<T>> visited = new Queue<Vertex<T>>(this.vertices.size());

    srcVertex.cost = 0.0;
    srcVertex.visited = true;
    visited.push(srcVertex);

    while (!visited.empty()) {
      Vertex<T> vertex = visited.front();
      visited.pop();

      if (vertex.equals(dstVertex)) {
        for (Vertex<T> current = vertex; current != srcVertex.prev; current = current.prev) {
          path.push(current);
        }
        path.reverse();
        break;
      }
      
      for (Vertex<T> neighbor : vertex.neighbors) {
        if (!neighbor.visited) {
          neighbor.cost = vertex.cost + this.getEdge(vertex.key, neighbor.key).weight;
          neighbor.prev = vertex;
          neighbor.visited = true;
          visited.push(neighbor);
        }
      }
    }

    path.reverse();
    return path;
  }

  public Array<Vertex<T>> dijkstra(T src, T dst) {
    Vertex<T> srcVertex = this.getVertex(src);
    Vertex<T> dstVertex = this.getVertex(dst);
    assert srcVertex != null && dstVertex != null;

    this.markAllVerticesUnvisited();
    Array<Vertex<T>> path = new Array<Vertex<T>>(this.vertices.size());
    pQueueHeap<Vertex<T>> visited = new pQueueHeap<Vertex<T>>(this.vertices.size());

    srcVertex.cost = 0.0;
    visited.push(srcVertex, srcVertex.cost);

    while (!visited.empty()) {
      Vertex<T> vertex = visited.top().getKey();
      vertex.visited = true;
      visited.pop();

      if (vertex.equals(dstVertex)) {
        for (Vertex<T> current = vertex; current != srcVertex.prev; current = current.prev) {
          path.push(current);
        }
        path.reverse();
        break;
      }

      for (Vertex<T> neighbor : vertex.neighbors) {
        if (!neighbor.visited) {
          double cost = vertex.cost + this.getEdge(vertex.key, neighbor.key).weight;

          if (cost < neighbor.cost) {
            neighbor.cost = cost;
            neighbor.prev = vertex;
            visited.push(neighbor, neighbor.cost);
          }
        }
      }
    }

    return path;
  }

  // Heuristic estimator
  private Double heuristic(Vertex<T> src, Vertex<T> dst) {
    return 0.0;
  }

  public Array<Vertex<T>> astar(T src, T dst) {
    Vertex<T> srcVertex = this.getVertex(src);
    Vertex<T> dstVertex = this.getVertex(dst);
    assert srcVertex != null && dstVertex != null;

    this.markAllVerticesUnvisited();
    Array<Vertex<T>> path = new Array<Vertex<T>>(this.vertices.size());
    pQueueHeap<Vertex<T>> visited = new pQueueHeap<Vertex<T>>(this.vertices.size());

    srcVertex.cost = 0.0;
    visited.push(srcVertex, this.heuristic(srcVertex, dstVertex));

    while (!visited.empty()) {
      Vertex<T> vertex = visited.top().getKey();
      vertex.visited = true;
      visited.pop();

      if (vertex.equals(dstVertex)) {
        for (Vertex<T> current = vertex; current != srcVertex.prev; current = current.prev) {
          path.push(current);
        }
        path.reverse();
        break;
      }

      for (Vertex<T> neighbor : vertex.neighbors) {
        if (!neighbor.visited) {
          double cost = vertex.cost + this.getEdge(vertex.key, neighbor.key).weight;

          if (cost < neighbor.cost) {
            neighbor.cost = cost;
            neighbor.prev = vertex;
            visited.push(neighbor, neighbor.cost + this.heuristic(neighbor, dstVertex));
          }
        }
      }
    }

    return path;
  }

  @Override 
  public String toString() {
    int index;
    int vertices = this.vertices.size();
    int edges = this.edges.size();
    String repr = "{\n";

    index = 0;
    for (Vertex<T> vertex : this.vertices) {
      repr += "\t" + vertex + (index == vertices - 1 && edges == 0 ? "\n" : ", \n");
      index++;
    }

    index = 0;
    for (Edge<T> edge : this.edges) {
      repr += "\t" + edge + (index == edges - 1 ? "\n" : ", \n");
      index++;
    }

    repr += "}";
    return repr;
  }
}

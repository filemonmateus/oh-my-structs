import java.util.Set;
import java.util.LinkedHashSet;

@SuppressWarnings("unchecked")
public class Vertex<T> implements Comparable<Vertex<T>> {
  public T key;
  public Vertex<T> prev;
  public Double cost;
  public boolean visited;
  public Set<Vertex<T>> neighbors;

  public Vertex(T key) {
    this(key, Double.MAX_VALUE);
  }

  public Vertex(T key, Double cost) {
    this.key = key;
    this.prev = null;
    this.cost = cost;
    this.visited = false;
    this.neighbors = new LinkedHashSet<Vertex<T>>();
  }

  public int compareTo(Vertex that) {
    return this.toString().compareTo(that.toString());
  }

  @Override
  public String toString() {
    String repr = "";
    repr += this.key;
    return repr;
  }

  @Override
  public int hashCode() {
    return this.key.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Vertex)) return this.key.equals(other);
    Vertex<T> node = (Vertex<T>) other;
    return this.key.equals(node.key);
  }
}

@SuppressWarnings("unchecked")
public class Stack<T> {
  private int size;
  private int capacity;
  private T[] elements;

  public Stack() { this(100); }

  public Stack(int capacity) {
    this.size = 0;
    this.capacity = capacity;
    this.elements = (T[]) new Object[this.capacity];
  }

  public void push(T element) {
    if (this.full()) this.resize();
    this.elements[this.size++] = element;
  }

  public void pop() {
    assert !this.empty();
    --this.size;
  }

  public void resize() {
    this.capacity *= 2;
    T[] elements = (T[]) new Object[this.capacity];
    for (int i = 0; i < this.size; ++i) {
      elements[i] = this.elements[i];
    }
    this.elements = elements;
  }

  public T top() {
    assert !this.empty();
    return this.elements[this.size-1];
  }

  public int size() {
    return this.size;
  }

  public boolean empty() {
    return this.size == 0;
  }

  public boolean full() {
    return this.size == this.capacity;
  }

  @Override
  public String toString() {
    String repr = "";
    for (int i = this.size-1; i >= 0; --i) {
      repr += this.elements[i] + " ";
    }
    return repr;
  }
}

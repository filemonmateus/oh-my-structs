public class Driver {
  public static void main(String[] args) {
    // init graph
    Graph<String> graph = new Graph<String>();

    // add airports
    graph.insertVertex("HNL");
    graph.insertVertex("SFO");
    graph.insertVertex("LAX");
    graph.insertVertex("ORD");
    graph.insertVertex("DFW");
    graph.insertVertex("PVD");
    graph.insertVertex("LGA");
    graph.insertVertex("MIA");

    // then flights and related routes
    graph.insertEdge("HNL", "MIA", 500.0);
    graph.insertEdge("HNL", "SFO", 130.0);
    graph.insertEdge("HNL", "LAX", 250.0);
    graph.insertEdge("SFO", "HNL", 130.0);
    graph.insertEdge("SFO", "LAX", 60.0);
    graph.insertEdge("SFO", "ORD", 70.0);
    graph.insertEdge("LAX", "HNL", 250.0);
    graph.insertEdge("LAX", "SFO", 60.0);
    graph.insertEdge("LAX", "ORD", 170.0);
    graph.insertEdge("LAX", "DFW", 120.0);
    graph.insertEdge("ORD", "SFO", 70.0);
    graph.insertEdge("ORD", "LAX", 170.0);
    graph.insertEdge("ORD", "DFW", 80.0);
    graph.insertEdge("ORD", "PVD", 50.0);
    graph.insertEdge("DFW", "LAX", 120.0);
    graph.insertEdge("DFW", "ORD", 80.0);
    graph.insertEdge("DFW", "LGA", 140.0);
    graph.insertEdge("DFW", "MIA", 110.0);
    graph.insertEdge("PVD", "ORD", 50.0);
    graph.insertEdge("PVD", "LGA", 200.0);
    graph.insertEdge("LGA", "DFW", 140.0);
    graph.insertEdge("LGA", "MIA", 100.0);
    graph.insertEdge("MIA", "DFW", 110.0);
    graph.insertEdge("MIA", "HNL", 500.0);

    // inspect cheapest route from San Francisco to Miami with A*
    System.out.println("Cheapest route (SFO->MIA) via A*");
    for (Vertex<String> vertex : graph.astar("SFO", "MIA")) {
      if (vertex.prev != null) {
        Double cost = vertex.cost - vertex.prev.cost;
        System.out.println(vertex.prev + "->" + vertex + " $(" + cost + ")");
      }
    }
    System.out.println();

    // inspect cheapest route from San Francisco to Miami with Dijkstra
    System.out.println("Cheapest Dijkstra route (SFO->MIA)");
    for (Vertex<String> vertex : graph.dijkstra("SFO", "MIA")) {
      if (vertex.prev != null) {
        Double cost = vertex.cost - vertex.prev.cost;
        System.out.println(vertex.prev + "->" + vertex + " $(" + cost + ")");
      }
    }
    System.out.println();

    // inspect fastest route from San Francisco to Miami
    System.out.println("Fastest BFS route (SFO->MIA)");
    for (Vertex<String> vertex : graph.bfs("SFO", "MIA")) {
      if (vertex.prev != null) {
        Double cost = vertex.cost - vertex.prev.cost;
        System.out.println(vertex.prev + "->" + vertex + " $(" + cost + ")");
      }
    }
    System.out.println();

    // inspect random dfs route from San Francisco to Miami
    System.out.println("Random DFS route (SFO->MIA)");
    for (Vertex<String> vertex : graph.dfs("SFO", "MIA")) {
      if (vertex.prev != null) {
        Double cost = vertex.cost - vertex.prev.cost;
        System.out.println(vertex.prev + "->" + vertex + " $(" + cost + ")");
      }
    }
  }
}

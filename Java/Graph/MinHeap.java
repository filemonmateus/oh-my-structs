@SuppressWarnings("unchecked")
public class MinHeap<T extends Comparable<T>> extends Array<T> {

  public MinHeap() { super(10); }

  public MinHeap(int capacity) { super(capacity); }

  private void heapify(int idx) {
    int min = idx, left = 2*idx, right = 2*idx+1;
    if (left < this.size() && this.elements[min].compareTo(this.elements[left]) == 1) min = left;
    if (right < this.size() && this.elements[min].compareTo(this.elements[right]) == 1) min = right;
    if (min != idx) { super.swap(min, idx); this.heapify(min); }
  }

  private void minheap() {
    for (int i = this.size()/2; i >= 0; --i) this.heapify(i);
  }

  public void push(T element) {
    super.push(element);
    if (element.compareTo(this.top()) == -1) this.minheap();
  }

  public void pop() {
    super.remove(0);
    this.minheap();
  }

  public T top() {
    return super.get(0);
  }
}

public class Driver {
  public static void main(String[] args) {
    // init list
    SinglyLinkedList<Integer> intsMod5 = new SinglyLinkedList<Integer>();

    // append ints
    intsMod5.append(0);
    intsMod5.append(1);
    intsMod5.append(2);
    intsMod5.append(3);
    intsMod5.append(4);

    // inspect integers mod 5
    System.out.println(intsMod5.toString());

    // reveset list
    intsMod5.reverse();

    // inspect integers mod 5
    System.out.println(intsMod5);
  }
}

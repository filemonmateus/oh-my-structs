@SuppressWarnings("unchecked")
public class Node<T> {
  public T key;
  public Node<T> prev;
  public Node<T> next;

  public Node(T key) {
    this.key = key;
    this.prev = this.next = null;
  }
}

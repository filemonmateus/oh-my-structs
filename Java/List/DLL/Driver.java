public class Driver {
  public static void main(String[] args) {
    // init primes
    DoublyLinkedList<Integer> primes = new DoublyLinkedList<Integer>();

    // add a few
    primes.append(2);
    primes.append(3);
    primes.append(5);
    primes.append(7);

    // inspect them
    System.out.println(primes.toString());

    // reverse them
    primes.reverse();

    // inspect them (again!)
    System.out.println(primes);
  }
}

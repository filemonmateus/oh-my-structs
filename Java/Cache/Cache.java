import java.util.Map;
import java.util.HashMap;

public class Cache<T, E> {
  private int size;
  private int capacity;
  private Map<T, Element<T, E>> records;
  private DoublyLinkedList<Element<T, E>> elements;

  public Cache() {
    this(10);
  }

  public Cache(int capacity) {
    this.size = 0;
    this.capacity = capacity;
    this.records = new HashMap<T, Element<T, E>>();
    this.elements = new DoublyLinkedList<Element<T, E>>();
  }

  public E get(T key) {
    if (!this.exists(key)) return (E) null;
    Element<T, E> element = this.records.get(key);
    this.elements.remove(element);
    this.elements.append(element);
    return element.val;
  }

  public void put(T key, E val) {
    if (this.exists(key)) this.elements.remove(this.records.get(key));
    Element<T, E> element = new Element<T, E>(key, val);
    this.elements.append(element);
    this.records.put(key, element);

    if (this.records.size() > this.capacity) {
      element = this.elements.head().key;
      this.elements.remove(element);
      this.records.remove(element.key);
    }
  }

  public boolean exists(T key) {
    return this.records.get(key) != null;
  }

  public int size() {
    return this.size;
  }

  @Override
  public String toString() {
    String repr = "";
    for (Node<Element<T, E>> current = this.elements.head(); current!= null; current = current.next) {
      repr += current.key.toString() + "->";  
    }
    repr += "null";
    return repr;
  }
}

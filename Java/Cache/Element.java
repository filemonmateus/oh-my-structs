@SuppressWarnings("unchecked")
public class Element<T, E> {
  public T key;
  public E val;

  public Element(T key, E val) {
    this.key = key;
    this.val = val;
  }

  @Override
  public String toString() {
    return "{" + this.key + ":" + this.val + "}";
  }

  @Override
  public int hashCode() {
    return this.key.hashCode() ^ this.val.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Element)) return false;
    Element<T, E> element = (Element<T, E>) other;
    return this.key == element.key && this.val == element.val;
  }
}

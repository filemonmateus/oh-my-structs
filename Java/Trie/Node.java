@SuppressWarnings("unchecked")
public class Node {
  public boolean word;
  public int ALPHABET = 26;
  public Node[] children;

  public Node () {
    this.children = new Node[this.ALPHABET];
    for (int i = 0; i < this.ALPHABET; ++i) {
      this.children[i] = null;
    }
    this.word = false;
  }
}

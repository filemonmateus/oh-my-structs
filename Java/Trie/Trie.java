import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

@SuppressWarnings("unchecked")
public class Trie {
  private Node root;
  private int words;

  public Trie () {
    this.root = new Node();
    this.words = 0;
  }

  public Trie (String dictionary) {
    this();
    this.load(dictionary);
  }

  public void add (String word) {
    word.toLowerCase();
    Node current = this.root;

    for (int i = 0; i < word.length(); ++i) {
      int idx = (int) word.charAt(i) - (int) 'a';
      if (current.children[idx] == null) current.children[idx] = new Node();
      current = current.children[idx];
    }

    current.word = true;
    this.words++;
  }

  public boolean check (String word) {
    word.toLowerCase();
    Node current = this.root;

    for (int i = 0; i < word.length(); ++i) {
      int idx = (int) word.charAt(i) - (int) 'a';
      if (current.children[idx] == null) return false;
      current = current.children[idx];
    }

    return current.word;
  } 

  public void load (String dictionary) {
    File dict = null;
    Scanner reader = null;

    try {
      dict = new File(dictionary);
      reader = new Scanner(dict);
    }
    catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
      return;
    }

    while(reader.hasNext()) {
      this.add(reader.next());
    }

    reader.close();
  }

  public boolean empty () {
    return this.words == 0;
  }

  public int size () {
    return this.words;
  }

  private void _autocomplete (String prefix, Node current) {
    if (current.word) System.out.println(prefix); 
    for (int i = 0; i < this.root.ALPHABET; ++i) {
      if (current.children[i] != null) {
        prefix += (char) (i + (int) 'a');
        this._autocomplete(prefix, current.children[i]);
        prefix = prefix.substring(0, prefix.length() - 1);
      }
    } 
  }

  public void autocomplete (String prefix) {
    Node current = this.root;
    for (int i = 0; i < prefix.length(); ++i) {
      int idx = (int) prefix.charAt(i) - (int) 'a';
      current = current.children[idx];
    }
    this._autocomplete(prefix, current);
  }

  private void _unload (Node current) {
    if (current == null) return;
    for (int i = 0; i < this.root.ALPHABET; ++i) {
      if (current.children[i] != null) {
        this._unload(current.children[i]);
      }
    }
    current = null;
  }

  public void unload () {
    this._unload(this.root);
    this.root = new Node();
    this.words = 0;
  }
}

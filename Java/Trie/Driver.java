/*
 * Oh-my-trie: a minimally viable prefix trie with autocomplete
 */

public class Driver {
  public static void main (String[] args) {
    // load dict.txt (3.69Mb) into memory
    Trie trie = new Trie("Dict.txt");

    // exhaustively search all matches for query "file"
    trie.autocomplete("file");

    // unload dict.txt (optional!)
    trie.unload();
  }
}

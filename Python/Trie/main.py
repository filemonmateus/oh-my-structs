import time
from trie import Trie

def main():
    tic = time.time()
    trie = Trie("dict.txt")
    trie.autocomplete("file")
    toc = time.time()
    print(f"Took: {toc-tic:.3f} seconds.")

if __name__ == '__main__':
    main()

from typing import Dict, Optional

ALPHABET: int = 26

class Node:

    def __init__(self) -> None:
        self.children: Dict[int, Optional[Node]] = {}
        self.word: bool = False
        for idx in range(ALPHABET):
            self.children[idx] = None


class Trie:

    def __init__(self, dictionary: Optional[str] = None) -> None:
        self.words: int = 0
        self.root: Optional[Node] = Node()
        if dictionary: self.load(dictionary)

    def __len__(self) -> int:
        return self.words

    def add(self, word: str) -> None:
        curr: Optional[Node] = self.root
        for letter in word:
            idx: int = ord(letter.lower()) - ord('a')
            if curr and curr.children[idx] is None: curr.children[idx] = Node()
            if curr: curr = curr.children[idx]
        if curr: curr.word = True
        self.words += 1

    def check(self, word: str) -> bool:
        curr: Optional[Node] = self.root
        for letter in word:
            idx: int = ord(letter.lower()) - ord('a')
            if curr and curr.children[idx] == None: return False
            if curr: curr = curr.children[idx]
        return curr.word if curr else False

    def load(self, dictionary: str) -> None:
        with open(dictionary, 'r') as dict:
            for word in dict:
                self.add(word.strip())

    def empty(self) -> bool:
        return len(self) == 0

    def _autocomplete(self, prefix: str, curr: Optional[Node]) -> None:
        if curr and curr.word: print(prefix)
        for idx in range(ALPHABET):
            if curr and curr.children[idx]:
                prefix += chr(idx+ord('a'))
                self._autocomplete(prefix, curr.children[idx])
                prefix = prefix[:-1]

    def autocomplete(self, prefix: str) -> None:
        curr: Optional[Node] = self.root
        for idx in range(len(prefix)):
            idx = ord(prefix[idx].lower()) - ord('a')
            curr = curr.children[idx] if curr else None
        self._autocomplete(prefix, curr)

    def _unload(self, curr: Optional[Node]) -> None:
        if curr:
            for idx in range(ALPHABET):
                if curr.children[idx] is None:
                    self._unload(curr.children[idx])
            del curr

    def unload(self) -> None:
        self._unload(self.root)
        self.root = Node()
        self.words = 0

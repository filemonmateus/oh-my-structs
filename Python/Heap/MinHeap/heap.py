from vector import Vector
from typing import Optional, TypeVar
from typing_extensions import Protocol

T = TypeVar('T', bound='Comparable')

class Comparable(Protocol):

    def __lt__(self, other) -> bool: ...


class MinHeap(Vector[T]):

    def __init__(self, capacity: int = 10) -> None:
        super().__init__(capacity)

    def minheap(self) -> None:
        for idx in range(len(self) // 2, -1, -1):
            self.heapify(idx)

    def heapify(self, idx: int) -> None:
        min: int = idx; left: int = 2*idx; right: int = 2*idx+1
        if left < len(self) and self.elements[left] < self.elements[min]: min = left # type: ignore
        if right < len(self) and self.elements[right] < self.elements[min]: min = right # type: ignore
        if idx != min:
            self.elements[idx], self.elements[min] = self.elements[min], self.elements[idx]
            self.heapify(min)

    def push(self, element: Optional[T]) -> None:
        super().push(element)
        if element and element < self.top(): self.minheap()

    def pop(self) -> None:
        super().remove(0)
        self.minheap()

    def top(self) -> Optional[T]:
        return super().get(0)

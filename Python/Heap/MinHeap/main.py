from heap import MinHeap

def main():
    minheap = MinHeap[int]()
    minheap.push(4)
    minheap.push(1)
    minheap.push(3)
    minheap.push(7)
    minheap.push(9)
    print(minheap)

if __name__ == '__main__':
    main()

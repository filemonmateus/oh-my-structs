from heap import MaxHeap

def main():
    maxheap = MaxHeap[int]()
    maxheap.push(4)
    maxheap.push(9)
    maxheap.push(3)
    maxheap.push(1)
    maxheap.push(7)
    print(maxheap)

if __name__ == '__main__':
    main()

from vector import Vector
from typing import Optional, TypeVar
from typing_extensions import Protocol

T = TypeVar('T', bound='Comparable')

class Comparable(Protocol):

    def __gt__(self, other) -> bool: ...


class MaxHeap(Vector[T]):

    def __init__(self, capacity: int = 10) -> None:
        super().__init__(capacity)

    def maxheap(self) -> None:
        for idx in range(len(self) // 2, -1, -1):
            self.heapify(idx)

    def heapify(self, idx: int) -> None:
        max: int = idx; left: int = 2*idx; right: int = 2*idx+1
        if left < len(self) and self.elements[left] > self.elements[max]: max = left # type: ignore
        if right < len(self) and self.elements[right] > self.elements[max]: max = right # type: ignore
        if idx != max:
            self.elements[idx], self.elements[max] = self.elements[max], self.elements[idx]
            self.heapify(max)

    def push(self, element: Optional[T]) -> None:
        super().push(element)
        if element and element > self.top(): self.maxheap()

    def pop(self) -> None:
        super().remove(0)
        self.maxheap()

    def top(self) -> Optional[T]:
        return super().get(0)

from vector import Vector

def main():
    # init vector
    v = Vector[int]()

    # append ints
    v.push(2)
    v.push(3)
    v.push(5)
    v.push(7)
    v.push(11)
    v.push(13)

    # insert ints
    v.insert(0,1)
    v.insert(3,4)
    v.insert(5,6)

    # remove ints
    v.remove(len(v)-1)
    v.remove(len(v)-1)

    # inspect vector
    print(v)

if __name__ == '__main__':
    main()

from __future__ import annotations
from typing import Any, Generic, Iterator, List, Optional, overload, Sequence, TypeVar, Union

T = TypeVar('T')

class Vector(Generic[T]):

    def __init__(self, capacity: int = 1) -> None:
        assert capacity > 0, ':< oops, capacity must be a strictly positive int!'
        self.idx: int = 0; self.size: int = 0; self.capacity: int = capacity
        self.elements: List[Optional[T]] = [None] * self.capacity

    def __len__(self) -> int:
        return self.size

    def __eq__(self, other: Any) -> bool:
        return self.elements == other.elements if isinstance(other, Vector) else False

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}('
        for idx in range(self.size):
            repr += f'{self.elements[idx]}' if idx == self.size - 1 else f'{self.elements[idx]} '
        return repr + ')'

    @overload
    def __getitem__(self, idx: int) -> Optional[T]: ...

    @overload
    def __getitem__(self, idx: slice) -> Vector[T]: ...

    def __getitem__(self, idx: Union[int, slice]) -> Union[Optional[T], Vector[T]]:
        if isinstance(idx, int):
            if idx >= self.size: raise StopIteration
            return self.elements[idx]
        else:
            # mimics the slicing behavior of generic lists in python
            sliced_xrange: Sequence[int] = range(self.size)[idx]
            sliced_vector: Vector[T] = Vector[T](len(sliced_xrange))
            for i in sliced_xrange:
                sliced_vector.push(self.__getitem__(i))
            return sliced_vector

    def __setitem__(self, idx: int, element: Optional[T]) -> None:
        if idx >= self.size: raise IndexError(f':< oops, index {idx} out of bounds!')
        self.elements[idx] = element

    def __next__(self) -> Optional[T]:
        element = self.__getitem__(self.idx)
        self.idx += 1
        return element

    def __iter__(self) -> Iterator[Optional[T]]:
        for idx in range(self.size):
            yield self.elements[idx]

    def empty(self) -> bool:
        return len(self) == 0

    def full(self) -> bool:
        return len(self) == self.capacity

    def resize(self) -> None:
        self.capacity *= 2
        elements: List[Optional[T]] = [None] * self.capacity
        for i in range(self.size):
            elements[i] = self.elements[i]
        del self.elements
        self.elements = elements

    def push(self, element: Optional[T]) -> None:
        if self.full(): self.resize()
        self.elements[self.size] = element
        self.size += 1

    def insert(self, idx: int, element: Optional[T]) -> None:
        assert idx < self.capacity, f':< oops, index {idx} out of bounds!'
        for i in range(self.size-1, idx, -1):
            self.elements[i] = self.elements[i-1]
        self.elements[idx] = element
        self.size += 1

    def remove(self, idx: int) -> None:
        assert idx < self.size
        for i in range(idx, self.size-1):
            self.elements[i] = self.elements[i+1]
        self.size -= 1

    def erase(self, element: Optional[T]) -> None:
        for idx in range(self.size):
            if self.elements[idx] == element:
                self.remove(idx)

    def reverse(self) -> None:
        for i in range(self.size//2):
            self.elements[i], self.elements[self.size-i-1] = self.elements[self.size-i-1], self.elements[i]

    def get(self, idx: int) -> Optional[T]:
        return self.__getitem__(idx)

    def set(self, idx: int, element: Optional[T]) -> None:
        self.__setitem__(idx, element)

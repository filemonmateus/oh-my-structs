from sll import Node
from sll import SinglyLinkedList
from typing import Generic, Optional, Tuple, TypeVar

T = TypeVar('T')

class pQueueList(SinglyLinkedList[Tuple[T, float]]):

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}('
        curr: Optional[Node[Tuple[T, float]]] = self.head

        while curr:
            repr += f'[{curr.key[0]}: {curr.key[1]}] ' if curr.next else f'[{curr.key[0]}: {curr.key[1]}]'
            curr = curr.next

        return repr + ')'

    def push(self, key: T, priority: float) -> None:
        if self.head is None or (self.head and priority > self.head.key[1]): return self.prepend((key, priority))
        curr: Optional[Node[Tuple[T, float]]] = self.head.next
        pred: Node[Tuple[T, float]] = self.head

        while curr:
            if curr.key[1] > priority:
                pred = curr
                curr = curr.next
            else:
                node: Node[Tuple[T, float]] = Node((key, priority))
                pred.next = node
                node.next = curr
                self.size += 1
                return

        self.append((key, priority))

    def pop(self) -> None:
        if self.head: self.remove(self.head.key)

    def top(self) -> Optional[Tuple[T, float]]:
        return self.head.key if self.head else None

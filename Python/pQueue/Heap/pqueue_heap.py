from heap import MaxHeap
from typing import Any, Generic, Optional, TypeVar, Union

T = TypeVar('T')

class Entry(Generic[T]):

    def __init__(self, key: T, priority: float) -> None:
        self.key: T = key
        self.priority: float = priority

    def __repr__(self) -> str:
        return f'({self.key}, {self.priority})'

    def __lt__(self, other: Any) -> bool:
        if isinstance(other, type(self)): return self.priority < other.priority
        raise TypeError(f':< oops, got {type(other).__name__} but expected {type(self).__name__}!')

    def __le__(self, other: Any) -> bool:
        if isinstance(other, type(self)): return self.priority <= other.priority
        raise TypeError(f':< oops, got {type(other).__name__} but expected {type(self).__name__}!')

    def __gt__(self, other: Any) -> bool:
        if isinstance(other, type(self)): return self.priority > other.priority
        raise TypeError(f':< oops, got {type(other).__name__} but expected {type(self).__name__}!')

    def __ge__(self, other: Any) -> bool:
        if isinstance(other, type(self)): return self.priority >= other.priority
        raise TypeError(f':< oops, got {type(other).__name__} but expected {type(self).__name__}!')

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, type(self)): return self.key == other.key and self.priority == other.priority
        raise TypeError(f':< oops, got {type(other).__name__} but expected {type(self).__name__}!')

    def __getitem__(self, idx: int) -> Union[T, float]:
        if idx == 0: return self.key
        if idx == 1: return self.priority
        raise IndexError(f':< oops, index {idx} out of bounds!')


class pQueueHeap(MaxHeap[Entry[T]]):

    '''Liskov Substitution Principle: see https://github.com/python/mypy/issues/1237'''
    def push(self, key: T, priority: float) -> None: # type: ignore[override]
        super().push(Entry(key, priority))

    def pop (self) -> None:
        super().pop()

    def top(self) -> Optional[Entry[T]]:
        return super().top()

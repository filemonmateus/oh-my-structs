from pqueue_heap import pQueueHeap
from typing import List

# a general point class interface
class Point:
    def __init__(self, x: float, y: float) -> None:
        self.x: float = x
        self.y: float = y
        self.d: float = self.x * self.x + self.y * self.y

    def __repr__(self) -> str:
        return f'({self.x},{self.y})'


# returns k closests points from the origin
def kclosests(points: List[Point], k: int) -> List[Point]:
    assert k > 0
    if len(points) <= k: return points
    closests: List[Point] = []
    maxheap: pQueueHeap[Point] = pQueueHeap[Point]()

    for i in range(k):
        maxheap.push(points[i], points[i].d)

    for i in range(k, len(points)):
        if maxheap.top() and points[i].d < maxheap.top()[1]: # type: ignore
            maxheap.pop()
            maxheap.push(points[i], points[i].d)

    while not maxheap.empty():
        closests.append(maxheap.top()[0]) # type: ignore
        maxheap.pop()

    return closests

def main():
    points: List[Point] = []
    points.append(Point(4,4))
    points.append(Point(1,1))
    points.append(Point(6,6))
    points.append(Point(2,2))
    points.append(Point(0,0))
    points.append(Point(3,3))
    points.append(Point(5,5))
    print(points)
    closests: List[Point] = kclosests(points, 2)
    print(closests)

if __name__ == '__main__':
    main()

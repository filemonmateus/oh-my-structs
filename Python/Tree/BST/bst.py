from stack import Stack
from queue import Queue
from vector import Vector
from typing import Any, Generic, List, TypeVar, Optional
from typing_extensions import Protocol

T = TypeVar('T', bound='Comparable')

class Comparable(Protocol):
    def __lt__(self, other: T) -> bool: ...


class Node(Generic[T]):

    def __init__(self, key: T) -> None:
        self.key: T = key
        self.left: Optional[Node[T]] = None
        self.right: Optional[Node[T]] = None

    def __repr__(self) -> str:
        return str(self.key)

    def __eq__(self, other: Any) -> bool:
        return self.key == other.key if isinstance(other, Node) else False


class BinarySearchTree(Generic[T]):

    def __init__(self) -> None:
        self.size: int = 0
        self.root: Optional[Node[T]] = None

    def __len__(self) -> int:
        return self.size

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}('
        for idx, node in enumerate(self.inorder()):
            repr += f'{node}' if idx == self.size - 1 else f'{node} '
        return repr + ')'

    def empty(self) -> bool:
        return len(self) == 0

    def insert(self, key: T) -> None:
        if self.empty():
            self.root = Node(key)
            self.size += 1
            return

        curr: Optional[Node[T]] = self.root

        while curr:
            if key < curr.key:
                if curr.left:
                    curr = curr.left
                else:
                    curr.left = Node(key)
                    self.size += 1
                    return
            else:
                if curr.right:
                    curr = curr.right
                else:
                    curr.right = Node(key)
                    self.size += 1
                    return

    def lookup(self, key: T) -> bool:
        if self.empty(): return False
        curr: Optional[Node[T]] = self.root
        while curr:
            if key == curr.key: return True
            elif key < curr.key: curr = curr.left
            else: curr = curr.right
        return False

    def _height(self, curr: Optional[Node[T]]) -> int:
        return max(self._height(curr.left), self._height(curr.right)) + 1 if curr else 0

    def height(self) -> int:
        return self._height(self.root)

    def _get_min_subtree_node(self, curr: Optional[Node[T]]) -> Optional[Node[T]]:
        return self._get_min_subtree_node(curr.left) if curr and curr.left else curr

    def _get_max_subtree_node(self, curr: Optional[Node[T]]) -> Optional[Node[T]]:
        return self._get_max_subtree_node(curr.right) if curr and curr.right else curr

    def _remove(self, curr: Optional[Node[T]], key: T) -> Optional[Node[T]]:
        if curr is None: return curr
        elif key < curr.key: curr.left = self._remove(curr.left, key)
        elif key > curr.key: curr.right = self._remove(curr.right, key)
        else:
            if curr.left == None and curr.right == None:
                trash: Optional[Node[T]] = curr
                curr = None
                del trash

            elif curr.left == None:
                trash = curr
                curr = curr.left
                del trash

            elif curr.right == None:
                trash = curr
                curr = curr.left
                del trash

            else:
                trash = self._get_min_subtree_node(curr.right)
                if trash: curr.key = trash.key
                self._remove(curr.right, curr.key)

        return curr

    def remove(self, key: T) -> None:
        self.root = self._remove(self.root, key)
        self.size -= 1

    def preorder(self) -> Vector[Optional[Node[T]]]:
        path: Vector[Optional[Node[T]]] = Vector(self.size)
        if self.empty(): return path
        stack: Stack[Optional[Node[T]]] = Stack(self.size)
        curr: Optional[Node[T]] = self.root
        stack.push(curr)

        while not stack.empty():
            curr = stack.top()
            stack.pop()
            path.push(curr)
            if curr and curr.right: stack.push(curr.right)
            if curr and curr.left: stack.push(curr.left)

        return path

    def inorder(self) -> Vector[Optional[Node[T]]]:
        path: Vector[Optional[Node[T]]] = Vector(self.size)
        if self.empty(): return path
        stack: Stack[Optional[Node[T]]] = Stack(self.size)
        curr: Optional[Node[T]] = self.root

        while not stack.empty() or curr:
            if curr:
                stack.push(curr)
                curr = curr.left
            else:
                curr = stack.top()
                stack.pop()
                path.push(curr)
                if curr: curr = curr.right

        return path

    def postorder(self) -> Vector[Optional[Node[T]]]:
        path: Vector[Optional[Node[T]]] = Vector(self.size)
        if self.empty(): return path
        stack1: Stack[Optional[Node[T]]] = Stack(self.size)
        stack2: Stack[Optional[Node[T]]] = Stack(self.size)
        curr: Optional[Node[T]] = self.root
        stack1.push(curr)

        while not stack1.empty():
            curr = stack1.top()
            stack1.pop()
            stack2.push(curr)
            if curr and curr.left: stack1.push(curr.left)
            if curr and curr.right: stack1.push(curr.right)

        while not stack2.empty():
            path.push(stack2.top())
            stack2.pop()

        return path

    def levelorder(self) -> Vector[Optional[Node[T]]]:
        path: Vector[Optional[Node[T]]] = Vector(self.size)
        if self.empty(): return path
        queue: Queue[Optional[Node[T]]] = Queue(self.size)
        curr: Optional[Node[T]] = self.root
        queue.push(curr)

        while not queue.empty():
            curr = queue.front()
            queue.pop()
            path.push(curr)
            if curr and curr.left: queue.push(curr.left)
            if curr and curr.right: queue.push(curr.right)

        return path

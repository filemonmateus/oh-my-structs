from bst import BinarySearchTree

def main():
    # init tree
    tree = BinarySearchTree[int]()

    # construct tree
    tree.insert(10)
    tree.insert(6)
    tree.insert(14)
    tree.insert(4)
    tree.insert(8)
    tree.insert(12)
    tree.insert(16)

    # inspect tree
    print(tree.preorder())
    print(tree.inorder())
    print(tree.postorder())
    print(tree.levelorder())
    print(tree)

if __name__ == '__main__':
    main()

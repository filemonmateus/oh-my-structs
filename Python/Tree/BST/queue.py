from typing import Any, Generic, List, Optional, TypeVar

T = TypeVar('T')

class Queue(Generic[T]):

    def __init__(self, capacity: int = 10) -> None:
        assert capacity > 0, ':< oops, capacity must be a strictly positive int!'
        self.head: int = 0; self.tail: int = 0; self.capacity: int = capacity
        self.elements: List[Optional[T]] = [None] * self.capacity

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}('
        for idx in range(self.head, self.tail):
            repr += f'{self.elements[idx]}' if idx == self.tail - 1 else f'{self.elements[idx]} '
        return repr + ')'

    def __eq__(self, other: Any) -> bool:
        return self.elements == other.elements if isinstance(other, Queue) else False

    def __len__(self) -> int:
        return self.tail - self.head

    def full(self) -> bool:
        return len(self) == self.capacity

    def empty(self) -> bool:
        return len(self) == 0

    def resize(self) -> None:
        self.capacity *= 2
        elements: List[Optional[T]] = [None] * self.capacity
        for idx in range(len(self)):
            elements[idx] = self.elements[self.head+idx]
        del self.elements
        self.elements = elements

    def push(self, element: T) -> None:
        if self.full(): self.resize()
        self.elements[self.tail] = element
        self.tail += 1

    def front(self) -> Optional[T]:
        assert not self.empty(), ':< oops, can\'t call front on empty queue!'
        return self.elements[self.head]

    def pop(self) -> None:
        assert not self.empty(), ':< oops, can\'t call pop on empty queue!'
        self.head += 1

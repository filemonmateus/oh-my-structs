from rbt import RedBlackTree

def main():
    # init tree
    tree = RedBlackTree[int]()

    # construct tree
    tree.insert(12)
    tree.insert(5)
    tree.insert(15)
    tree.insert(3)
    tree.insert(10)
    tree.insert(13)
    tree.insert(17)
    tree.insert(4)
    tree.insert(7)
    tree.insert(11)
    tree.insert(14)
    tree.insert(6)
    tree.insert(8)

    # node deletion
    tree.remove(13)

    # inspect tree
    print(tree.preorder())
    print(tree.inorder())
    print(tree.postorder())
    print(tree.levelorder())
    print(tree.height())

if __name__ == '__main__':
    main()

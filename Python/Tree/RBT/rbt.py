from stack import Stack
from queue import Queue
from vector import Vector
from typing import Any, cast, Generic, Optional, TypeVar
from typing_extensions import Protocol

T = TypeVar('T', bound='Comparable')
RED = 'R'
BLACK = 'B'

class Comparable(Protocol):

    def __lt__(self, other):
        ...


class Node(Generic[T]):

    def __init__(self, key: T, color: str = RED) -> None:
        self.key: T = key
        self.color: str = color
        self.left: Optional[Node[T]] = None
        self.right: Optional[Node[T]] = None
        self.parent: Optional[Node[T]] = None

    def __repr__(self) -> str:
        repr: str = '('
        repr += f'{self.key}, {self.color}'
        repr += ')'
        return repr

    def __eq__(self, other: Any) -> bool:
        return self.key == other.key if isinstance(other, Node) else False


class RedBlackTree(Generic[T]):

    def __init__(self) -> None:
        self.size: int = 0
        self.root: Optional[Node[T]] = Node(cast(T, 0), BLACK); self.nill: Optional[Node[T]] = Node(cast(T, 0), BLACK)

    def __len__(self) -> int:
        return self.size

    def _height(self, current: Optional[Node[T]]) -> int:
        return 0 if current is None or current == self.nill else max(self._height(current.left), self._height(current.right)) + 1

    def height(self) -> int:
        return self._height(self.root)

    def empty(self) -> bool:
        return self.size == 0

    def lookup(self, key: T) -> Optional[Node[T]]:
        if self.empty(): return None
        current: Optional[Node[T]] = self.root
        while current:
            if key == current.key: return current
            elif key < current.key: current = current.left
            else: current = current.right
        return None

    def _transplant(self, this: Node[T], that: Node[T]) -> None:
        if this.parent is None or this.parent == self.nill: self.root = that
        elif this == this.parent.left: this.parent.left = that
        else: this.parent.right = that
        if this is not None and that is not None: that.parent = this.parent

    def _minimum(self, current: Optional[Node[T]]) -> Optional[Node[T]]:
        return self._minimum(current.left) if current and current.left else current

    def _maximum(self, current: Optional[Node[T]]) -> Optional[Node[T]]:
        return self._maximum(current.right) if current and current.right else current

    def _successor(self, current: Optional[Node[T]]) -> Optional[Node[T]]:
        if current is None: return None
        elif current.right: return self._minimum(current.right)
        parent: Optional[Node[T]] = current.parent
        while parent is not None and current != parent.right:
            current = parent
            parent = parent.parent
        return parent

    def _predecessor(self, current: Optional[Node[T]]) -> Optional[Node[T]]:
        if current is None: return None
        elif current.left: return self._maximum(current.left)
        parent: Optional[Node[T]] = current.parent
        while parent is not None and current == parent.left:
            current = parent
            parent = parent.parent
        return parent

    def insert(self, key: T) -> None:
        parent: Optional[Node[T]] = self.nill
        current: Optional[Node[T]] = self.root
        newnode: Node[T] = Node(key)

        while current and current != self.nill:
            parent = current
            if newnode.key < current.key:
                current = current.left
            else:
                current = current.right

        newnode.parent = parent

        if parent is None or parent == self.nill:
            self.root = newnode
        elif newnode.key < parent.key:
            parent.left = newnode
        else:
            parent.right = newnode

        newnode.left = self.nill
        newnode.right = self.nill
        newnode.color = RED
        self._insert_fixup(newnode)

    # TODO: add generic type annotation in this sub-routine
    def _insert_fixup(self, current):
        while current.parent.color == RED:
            if current.parent == current.parent.parent.left:
                uncle = current.parent.parent.right

                if uncle.color == RED:
                    current.parent.color = BLACK
                    uncle.color = BLACK
                    current.parent.parent.color = RED
                    current = current.parent.parent
                else:
                    if current == current.parent.right:
                        current = current.parent
                        self.left_rotate(current)

                    current.parent.color = BLACK
                    current.parent.parent.color = RED
                    self.right_rotate(current.parent.parent)

            else:
                uncle = current.parent.parent.left

                if uncle.color == RED:
                    current.parent.color = BLACK
                    uncle.color = BLACK
                    current.parent.parent.color = RED
                    current = current.parent.parent
                else:
                    if current == current.parent.left:
                        current = current.parent
                        self.right_rotate(current)

                    current.parent.color = BLACK
                    current.parent.parent.color = RED
                    self.left_rotate(current.parent.parent)

        self.root.color = BLACK
        self.size += 1

    # TODO: add generic type annotation in this sub-routine
    def remove(self, key):
        trash = self.lookup(key)
        if trash == None: return
        trash_copy = trash
        trash_copy_original_color = trash_copy.color
        child = self.nill

        if trash.left == self.nill:
            child = trash.right
            self._transplant(trash, trash.right)
        elif trash.right == self.nill:
            child = trash.left
            self._transplant(trash, trash.left)
        else:
            trash_copy = self._minimum(trash.right)
            trash_copy_original_color = trash_copy.color
            child = trash_copy.right

            if trash_copy.parent == trash:
                child.parent = trash
            else:
                self._transplant(trash_copy, trash_copy.right)
                trash_copy.right = trash.right
                trash_copy.right.parent = trash_copy

            self._transplant(trash, trash_copy)
            trash_copy.left = trash.left
            trash_copy.left.parent = trash_copy
            trash_copy.color = trash.color

        if trash_copy_original_color == BLACK:
            self._remove_fixup(child)

    # TODO: add generic type annotation in this sub-routine
    def _remove_fixup(self, current):
        while current and current != self.root and current.color == BLACK:
            if current == current.parent.left:
                sibling = current.parent.right

                if sibling.color == RED:
                    sibling.color = BLACK
                    current.parent.color = RED
                    self.left_rotate(current.parent)
                    sibling = current.parent.right

                if sibling.left.color == BLACK and sibling.right.color == BLACK:
                    sibling.color = RED
                    current = current.parent
                else:
                    if sibling.right.color == BLACK:
                        sibling.left.color = BLACK
                        sibling.color = RED
                        self.right_rotate(sibling)
                        sibling = current.parent.right

                    sibling.color = current.parent.color
                    current.parent.color = BLACK
                    sibling.right.color = BLACK
                    self.left_rotate(current.parent)
                    current = self.root

            else:
                sibling = current.parent.left

                if sibling.color == RED:
                    sibling.color = BLACK
                    current.parent.color = RED
                    self.right_rotate(current.parent)
                    sibling = current.parent.left

                if sibling.right.color == BLACK and sibling.left.color == BLACK:
                    sibling.color = RED
                    current = current.parent
                else:
                    if sibling.left.color == BLACK:
                        sibling.right.color = BLACK
                        sibling.color = RED
                        self.left_rotate(sibling)
                        sibling = current.parent.left

                    sibling.color = current.parent.color
                    current.parent.color = BLACK
                    sibling.left.color = BLACK
                    self.right_rotate(current.parent)
                    current = self.root

        if current: current.color = BLACK
        self.size -= 1

    # TODO: add generic type annotation in this sub-routine
    def left_rotate(self, current):
        child = current.right
        current.right = child.left
        if child.left != self.nill: child.left.parent = current
        child.parent = current.parent
        if current.parent == self.nill: self.root = child
        elif current == current.parent.left: current.parent.left = child
        else: current.parent.right = child
        child.left = current
        current.parent = child

    # TODO: add generic type annotation in this sub-routine
    def right_rotate(self, current):
        child = current.left
        current.left = child.right
        if child.right == self.nill: child.right.parent = current
        child.parent = current.parent
        if current.parent == self.nill: self.root = child
        elif current == current.parent.right: current.parent.right = child
        else: current.parent.left = child
        child.right = current
        current.parent = child

    def preorder(self) -> Vector[Optional[Node[T]]]:
        path: Vector[Optional[Node[T]]] = Vector(self.size)
        if self.empty(): return path
        stack: Stack[Optional[Node[T]]] = Stack(self.size)
        current: Optional[Node[T]] = self.root
        stack.push(current)

        while not stack.empty():
            current = stack.top()
            stack.pop()
            if current == self.nill: continue
            path.push(current)
            if current and current.right != self.nill: stack.push(current.right)
            if current and current.left != self.nill: stack.push(current.left)

        return path

    def inorder(self) -> Vector[Optional[Node[T]]]:
        path: Vector[Optional[Node[T]]] = Vector(self.size)
        if self.empty(): return path
        stack: Stack[Optional[Node[T]]] = Stack(self.size)
        current: Optional[Node[T]] = self.root

        while not stack.empty() or current != self.nill:
            if current and current != self.nill:
                stack.push(current)
                current = current.left
            else:
                current = stack.top()
                stack.pop()
                path.push(current)
                if current: current = current.right

        return path

    def postorder(self) -> Vector[Optional[Node[T]]]:
        path: Vector[Optional[Node[T]]] = Vector(self.size)
        if self.empty(): return path
        stack1: Stack[Optional[Node[T]]] = Stack(self.size)
        stack2: Stack[Optional[Node[T]]] = Stack(self.size)
        current: Optional[Node[T]] = self.root
        stack1.push(current)

        while not stack1.empty():
            current = stack1.top()
            stack1.pop()
            if current == self.nill: continue
            stack2.push(current)
            if current and current.left != self.nill: stack1.push(current.left)
            if current and current.right != self.nill: stack1.push(current.right)

        while not stack2.empty():
            path.push(stack2.top())
            stack2.pop()

        return path

    def levelorder(self) -> Vector[Optional[Node[T]]]:
        path: Vector[Optional[Node[T]]] = Vector(self.size)
        if self.empty(): return path
        queue: Queue[Optional[Node[T]]] = Queue(self.size)
        current: Optional[Node[T]] = self.root
        queue.push(current)

        while not queue.empty():
            current = queue.front()
            queue.pop()
            if current == self.nill: continue
            path.push(current)
            if current and current.left != self.nill: queue.push(current.left)
            if current and current.right != self.nill: queue.push(current.right)

        return path

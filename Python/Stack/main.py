from stack import Stack

def main():
    s = Stack[int]()
    s.push(1)
    s.push(2)
    s.push(3)
    s.push(4)
    print(s)
    s.pop()
    s.pop()
    s.pop()
    s.pop()

if __name__ == '__main__':
    main()

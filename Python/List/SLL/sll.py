from typing import Any, Generic, Optional, TypeVar

T = TypeVar('T')

class Node(Generic[T]):

    def __init__(self, key: T) -> None:
        self.key: T = key
        self.next: Optional[Node[T]] = None

    def __repr__(self) -> str:
        return str(self.key)

    def __eq__(self, other: Any) -> bool:
        return self.key == other.key if isinstance(other, Node) else False


class SinglyLinkedList(Generic[T]):

    def __init__(self) -> None:
        self.size: int = 0
        self.head: Optional[Node[T]] = None

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}('
        curr: Optional[Node[T]] = self.head

        while curr:
            repr += f'{curr} ' if curr.next else f'{curr}'
            curr = curr.next

        return repr + ')'

    def __len__(self) -> int:
        return self.size

    def empty(self) -> bool:
        return len(self) == 0

    def prepend(self, key: T) -> None:
        node: Node[T] = Node(key)
        node.next = self.head
        self.head = node
        self.size += 1

    def append(self, key: T) -> None:
        if self.head is not None: # we use this (instead of `not self.empty`) to prevent generic type annotation error from mypy
            curr: Optional[Node[T]] = self.head.next
            pred: Node[T] = self.head

            while curr:
                pred = curr
                curr = curr.next

            pred.next = curr = Node(key)
            self.size += 1

        else: self.prepend(key)


    def lookup(self, key: T) -> bool:
        curr: Optional[Node[T]] = self.head
        while curr:
            if curr.key == key:
                return True
            curr = curr.next
        return False

    def remove(self, key: T) -> None:
        if self.head is not None: # use this (instead of `not self.empty`) to prevent generic type annotation error from mypy
            if self.head.key == key:
                trash: Optional[Node[T]] = self.head
                self.head = self.head.next
                del trash
                self.size -= 1
                return

            trash = self.head.next
            pred: Node[T] = self.head

            while trash:
                if trash.key == key:
                    pred.next = trash.next
                    del trash
                    self.size -= 1
                    return

                pred = trash
                trash = trash.next

    def reverse(self):
        if self.head is None or self.head.next is None: return
        curr: Optional[Node[T]] = self.head
        pred: Optional[Node[T]] = None
        succ: Optional[Node[T]] = None

        while curr:
            succ = curr.next
            curr.next = pred
            pred = curr
            curr = succ

        self.head = pred

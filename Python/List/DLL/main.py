from dll import DoublyLinkedList

def main():
    list = DoublyLinkedList()
    list.append(1)
    list.append(2)
    list.append(3)
    list.append(4)
    list.append(5)
    print(list)
    list.reverse()
    print(list)

if __name__ == '__main__':
    main()

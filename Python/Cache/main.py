from cache import Cache

def main():
    cache = Cache[int,int](2)
    cache.put(1,1)
    cache.put(2,2)
    print(cache.get(1))
    cache.put(3,3)
    print(cache.get(2))
    cache.put(4,4)
    print(cache.get(1))
    print(cache.get(3))
    print(cache.get(4))
    print(cache)

if __name__ == '__main__':
    main()

from typing import Any, Generic, List, Optional, TypeVar

T = TypeVar('T')

class Node(Generic[T]):

    def __init__(self, key: T) -> None:
        self.key: T = key
        self.prev: Optional[Node[T]] = None
        self.next: Optional[Node[T]] = None

    def __repr__(self) -> str:
        return str(self.key)

    def __eq__(self, other: Any) -> bool:
        return self.key == other.key if isinstance(other, Node) else False


class DoublyLinkedList(Generic[T]):

    def __init__(self) -> None:
        self.size: int = 0
        self.head: Optional[Node[T]] = None
        self.tail: Optional[Node[T]] = None

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}('
        curr: Optional[Node[T]] = self.head

        while curr:
            repr += f'{curr} ' if curr.next else f'{curr}'
            curr = curr.next

        return repr + ')'

    def __len__(self) -> int:
        return self.size

    def empty(self) -> bool:
        return len(self) == 0

    def prepend(self, key: T) -> None:
        if self.head is None: # we use this (instead of `self.empty`) to prevent generic type annotation error from mypy
            self.head = self.tail = Node(key)
            self.size += 1
            return

        node: Node[T] = Node(key)
        node.next = self.head
        self.head.prev = node
        self.head = node
        self.size += 1

    def append(self, key: T) -> None:
        if self.tail is None: return self.prepend(key)
        node: Node[T] = Node(key)
        node.prev = self.tail
        self.tail.next = node
        self.tail = node
        self.size += 1

    def lookup(self, key: T) -> bool:
        curr: Optional[Node[T]] = self.head
        while curr:
            if curr.key == key:
                return True
            curr = curr.next
        return False

    def remove(self, key: T) -> None:
        if self.head:
            if self.head.key == key:
                trash: Optional[Node[T]] = self.head
                self.head = self.head.next
                if self.head and trash: self.head.prev = trash.prev
                del trash
                self.size -= 1
                return

            trash = self.head.next

            while trash:
                if trash.key == key:
                    if trash.prev: trash.prev.next = trash.next
                    if trash.next: trash.next.prev = trash.prev
                    del trash
                    self.size -= 1
                    return

    def reverse(self) -> None:
        if len(self) < 2: return
        curr: Optional[Node[T]] = self.head
        pred: Optional[Node[T]] = None

        while curr:
            pred = curr.prev
            curr.prev = curr.next
            curr.next = pred
            curr = curr.prev

        self.head = self.tail
        if pred: self.tail = pred.prev

from dll import Node
from dll import DoublyLinkedList
from typing import Any, Dict, Generic, Optional, TypeVar

K = TypeVar('K')
V = TypeVar('V')

class Entry(Generic[K,V]):

    def __init__(self, key: K, val: V) -> None:
        self.key: K = key
        self.val: V = val

    def __eq__(self, other: Any) -> bool:
        return self.key == other.key and self.val == other.val if isinstance(other, Entry) else False

    def __repr__(self) -> str:
        return f'[{self.key}:{self.val}]'


class Cache(Generic[K,V]):

    def __init__(self, capacity: int = 10) -> None:
        assert capacity > 0, ':< oops, capacity must be a strictly positive int!'
        self.entries: Dict[K, Entry[K,V]] = {}
        self.capacity: int = capacity
        self.elements : DoublyLinkedList[Entry[K,V]] = DoublyLinkedList()

    def __len__(self) -> int:
        return len(self.elements)

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}('
        curr: Optional[Node[Entry[K,V]]] = self.elements.head

        while curr:
            repr += f'{curr} ' if curr.next else f'{curr}'
            curr = curr.next

        return repr + ')'

    def exists(self, key: K) -> Optional[Entry[K,V]]:
        return self.entries.get(key, None)

    def get(self, key: K) -> Optional[V]:
        if not self.exists(key): return None
        entry: Entry[K,V] = self.entries[key]
        self.elements.remove(entry)
        self.elements.append(entry)
        return entry.val

    def put(self, key: K, val: V) -> None:
        if self.exists(key): self.elements.remove(self.entries[key])
        entry: Entry[K,V] = Entry(key, val)
        self.elements.append(entry)
        self.entries[key] = entry

        if self.capacity < len(self.entries) and self.elements.head: # prevents generic type annotation error from mypy (ideally just the 1st sub-statement would suffice)
            entry = self.elements.head.key
            self.elements.remove(entry)
            del self.entries[entry.key]

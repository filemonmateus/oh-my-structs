from queue import Queue

def main():
    # init queue
    queue = Queue[int]()

    # add values
    queue.push(1)
    queue.push(2)
    queue.push(3)
    queue.push(4)
    queue.push(5)

    # inspect queue
    print(queue)

    # check FIFO invariance
    print(f'front() -> {queue.front()}')
    queue.pop()
    print(f'front() -> {queue.front()}')
    queue.pop()

    # check queue state
    print(queue)

if __name__ == '__main__':
    main()

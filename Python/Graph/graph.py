from stack import Stack
from queue import Queue
from vector import Vector
from pqueue_list import pQueueList
from typing import Any, Callable, Generic, Optional, Set, TypeVar, Tuple, Union

T = TypeVar('T')

class Node(Generic[T]):

    def __init__(self, key: T, cost: float = float('inf')) -> None:
        self.key: T = key
        self.prev: Optional[Node[T]] = None
        self.cost: float = cost
        self.visited: bool = False
        self.neighbors: Set[Node[T]] = set()

    def __repr__(self) -> str:
        return f'{type(self).__name__}(\'{self.key}\')'

    def __eq__(self, other: Any) -> bool:
        return self.key == other.key if isinstance(other, Node) else False

    def __hash__(self) -> int:
        return sum([ord(char) for char in str(self.key)])


class Edge(Generic[T]):

    def __init__(self, src: Node[T], dst: Node[T], weight: float = 0.0) -> None:
        self.src: Node[T] = src
        self.dst: Node[T] = dst
        self.weight: float = weight
        self.src.neighbors.add(self.dst)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.src}->{self.dst}, {self.weight})'

    def __eq__(self, other: Any) -> bool:
        return self.src == other.src and self.dst == other.dst and self.weight == other.weight if isinstance(other, Edge) else False

    def __hash__(self) -> int:
        return hash(self.src) - hash(self.dst) + int(self.weight)

    def __del__(self) -> None:
        self.src.neighbors.remove(self.dst)


class Graph(Generic[T]):

    def __init__(self) -> None:
        self.nodes: Set[Node[T]] = set()
        self.edges: Set[Edge[T]] = set()

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}(\n'

        for node in self.nodes:
            repr += f'\t{node}\n'

        for edge in self.edges:
            repr += f'\t{edge}\n'

        repr += ')'
        return repr

    def get_node(self, key: Union[Node[T], Optional[T]]) -> Optional[Node[T]]:
        if isinstance(key, Node):
            return key
        for node in self.nodes:
            if node.key == key:
                return node
        return None

    def contains_node(self, key: Union[Node[T], Optional[T]]) -> bool:
        return self.get_node(key) != None

    def get_edge(self, src: Union[Node[T], Optional[T]], dst: Union[Node[T], Optional[T]], weight: float = 0.0) -> Optional[Edge[T]]:
        src_node: Optional[Node[T]] = self.get_node(src)
        dst_node: Optional[Node[T]] = self.get_node(dst)
        assert(src_node and dst_node)
        for edge in self.edges:
            if weight:
                if edge.src == src_node and edge.dst == dst_node and edge.weight == weight:
                    return edge
            else:
                if edge.src == src_node and edge.dst == dst_node:
                    return edge
        return None

    def contains_edge(self, src: Union[Node[T], Optional[T]], dst: Union[Node[T], Optional[T]], weight: float = 0.0) -> bool:
        return self.get_edge(src, dst, weight) != None

    def add_node(self, key: Union[Node[T], T]) -> None:
        if isinstance(key, Node): self.nodes.add(key)
        else: self.nodes.add(Node(key))

    def remove_node(self, key: Union[Node[T], T]) -> None:
        node = key if isinstance(key, Node) else self.get_node(key)
        if node:
            for edge in list(self.edges):
                if edge.src == node or edge.dst == node:
                    self.edges.remove(edge)
            self.nodes.remove(node)

    def add_edge(self, src: Union[Node[T], T], dst: Union[Node[T], T], weight: float = 0.0) -> None:
        src_node: Optional[Node[T]] = self.get_node(src)
        dst_node: Optional[Node[T]] = self.get_node(dst)
        assert(src_node and dst_node)
        if self.contains_edge(src_node, dst_node, weight): return
        self.edges.add(Edge(src_node, dst_node, weight))

    def remove_edge(self, src: Union[Node[T], Optional[T]], dst: Union[Node[T], Optional[T]], weight: float = 0.0) -> None:
        edge: Optional[Edge[T]] = self.get_edge(src, dst, weight)
        if edge:
            self.edges.remove(edge)

    def get_nodes(self) -> Set[Node[T]]:
        return self.nodes

    def get_edges(self) -> Set[Edge[T]]:
        return self.edges

    def get_neighbors(self, key: Union[Node[T], T]) -> Set[Node[T]]:
        node: Optional[Node[T]] = self.get_node(key)
        return node.neighbors if node else set()

    def mark_all_nodes_unvisited(self) -> None:
        for node in self.nodes:
            node.prev = None
            node.cost = float('inf')
            node.visited = False

    def dfs(self, src: Union[Node[T], T], dst: Union[Node[T], Optional[T]] = None) -> Vector[Optional[Node[T]]]:
        src_node: Optional[Node[T]] = self.get_node(src)
        dst_node: Optional[Node[T]] = self.get_node(dst)
        assert(src_node)

        self.mark_all_nodes_unvisited()
        visited: Stack[Optional[Node[T]]] = Stack(len(self.nodes))
        path: Vector[Optional[Node[T]]] = Vector(len(self.nodes))
        src_node.cost = 0.0
        src_node.visited = True
        visited.push(src_node)

        while not visited.empty():
            node: Optional[Node[T]] = visited.top()
            if node is None: continue
            visited.pop()
            if dst_node is None:
                path.push(node)
            elif node == dst_node:
                curr: Optional[Node[T]] = dst_node
                while curr and curr != src_node.prev:
                    path.push(curr)
                    curr = curr.prev
                path.reverse()
                return path

            for neighbor in node.neighbors:
                if not neighbor.visited:
                    edge: Optional[Edge[T]] = self.get_edge(node, neighbor)
                    neighbor.cost = node.cost + edge.weight if edge else 0.0
                    neighbor.prev = node
                    neighbor.visited = True
                    visited.push(neighbor)

        return path

    def bfs(self, src: Union[Node[T], T], dst: Union[Node[T], Optional[T]] = None) -> Vector[Optional[Node[T]]]:
        src_node: Optional[Node[T]] = self.get_node(src)
        dst_node: Optional[Node[T]] = self.get_node(dst)
        assert(src_node)

        self.mark_all_nodes_unvisited()
        visited: Queue[Optional[Node[T]]] = Queue(len(self.nodes))
        path: Vector[Optional[Node[T]]] = Vector(len(self.nodes))
        src_node.cost = 0.0
        src_node.visited = True
        visited.push(src_node)

        while not visited.empty():
            node: Optional[Node[T]] = visited.front()
            if node is None: continue
            visited.pop()
            if dst_node is None:
                path.push(node)
            elif node == dst_node:
                curr: Optional[Node[T]] = dst_node
                while curr and curr != src_node.prev:
                    path.push(curr)
                    curr = curr.prev
                path.reverse()
                return path

            for neighbor in node.neighbors:
                if not neighbor.visited:
                    edge: Optional[Edge[T]] = self.get_edge(node, neighbor)
                    neighbor.cost = node.cost + edge.weight if edge else 0.0
                    neighbor.prev = node
                    neighbor.visited = True
                    visited.push(neighbor)

        return path

    def dijkstra(self, src: Union[Node[T], T], dst: Union[Node[T], T]) -> Vector[Optional[Node[T]]]:
        src_node: Optional[Node[T]] = self.get_node(src)
        dst_node: Optional[Node[T]] = self.get_node(dst)
        assert(src_node and dst_node)

        self.mark_all_nodes_unvisited()
        visited: pQueueList[Optional[Node[T]]] = pQueueList()
        path: Vector[Optional[Node[T]]] = Vector(len(self.nodes))
        src_node.cost = 0.0
        visited.push(src_node, src_node.cost)

        while not visited.empty():
            top: Optional[Tuple[Optional[Node[T]], float]] = visited.top()
            node: Optional[Node[T]] = top[0] if top else None
            if node is None: continue
            node.visited = True
            visited.pop()
            if node == dst_node:
                curr: Optional[Node[T]] = dst_node
                while curr and curr != src_node.prev:
                    path.push(curr)
                    curr = curr.prev
                path.reverse()
                return path

            for neighbor in node.neighbors:
                if not neighbor.visited:
                    edge: Optional[Edge[T]] = self.get_edge(node, neighbor)
                    cost: float = node.cost + edge.weight if edge else 0.0
                    if cost < neighbor.cost:
                        neighbor.cost = cost
                        neighbor.prev = node
                        visited.push(neighbor, neighbor.cost)

        return path

    def astar(self, src: Union[Node[T], T], dst: Union[Node[T], T], heuristic: Optional[Callable[[Node[T], Node[T]], float]] = None) -> Vector[Optional[Node[T]]]:
        src_node: Optional[Node[T]] = self.get_node(src)
        dst_node: Optional[Node[T]] = self.get_node(dst)
        assert(src_node and dst_node)

        self.mark_all_nodes_unvisited()
        visited: pQueueList[Optional[Node[T]]] = pQueueList()
        path: Vector[Optional[Node[T]]] = Vector(len(self.nodes))
        src_node.cost = 0.0
        visited.push(src_node, src_node.cost + heuristic(src_node, dst_node) if heuristic else src_node.cost)

        while not visited.empty():
            top: Optional[Tuple[Optional[Node[T]], float]] = visited.top()
            node: Optional[Node[T]] = top[0] if top else None
            if node is None: continue
            node.visited = True
            visited.pop()
            if node == dst_node:
                curr: Optional[Node[T]] = dst_node
                while curr and curr != src_node.prev:
                    path.push(curr)
                    curr = curr.prev
                path.reverse()
                return path

            for neighbor in node.neighbors:
                if not neighbor.visited:
                    edge: Optional[Edge[T]] = self.get_edge(node, neighbor)
                    cost: float = node.cost + edge.weight if edge else 0.0
                    if cost < neighbor.cost:
                        neighbor.cost = cost
                        neighbor.prev = node
                        visited.push(neighbor, neighbor.cost + heuristic(neighbor, dst_node) if heuristic else neighbor.cost)

        return path

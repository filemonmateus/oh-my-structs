from typing import Any, Generic, List, Optional, TypeVar

T = TypeVar('T')

class Stack(Generic[T]):

    def __init__(self, capacity: int = 1) -> None:
        assert capacity > 0, ':< oops, capacity must be a strictly positive int!'
        self.size: int = 0; self.capacity: int = capacity
        self.elements: List[Optional[T]] = [None] * self.capacity

    def __len__(self) -> int:
        return self.size

    def __eq__(self, other: Any) -> bool:
        return self.elements == other.elements if isinstance(other, Stack) else False

    def __repr__(self) -> str:
        repr: str = f'{type(self).__name__}('
        for idx in range(self.size):
            repr += f'{self.elements[self.size-idx-1]}' if idx == self.size - 1 else f'{self.elements[self.size-idx-1]} '
        return repr + ')'

    def full(self) -> bool:
        return len(self) == self.capacity

    def empty(self) -> bool:
        return len(self) == 0

    def resize(self) -> None:
        self.capacity *= 2
        elements: List[Optional[T]] = [None] * self.capacity
        for idx in range(self.size):
            elements[idx] = self.elements[idx]
        del self.elements
        self.elements = elements

    def push(self, element: T) -> None:
        if self.full(): self.resize()
        self.elements[self.size] = element
        self.size += 1

    def top(self) -> Optional[T]:
        assert not self.empty(), ':< oops, can\'t top on empty stack!'
        return self.elements[self.size-1]

    def pop(self) -> None:
        assert not self.empty(), ':< oops, can\'t pop on empty stack!'
        self.size -= 1

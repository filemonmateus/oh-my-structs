import time
from graph import Graph

def main():
    tic = time.time()
    graph = Graph()

    # add airports
    graph.add_node("HNL")
    graph.add_node("SFO")
    graph.add_node("LAX")
    graph.add_node("ORD")
    graph.add_node("DFW")
    graph.add_node("PVD")
    graph.add_node("LGA")
    graph.add_node("MIA")

    # then flights and related routes
    graph.add_edge("HNL", "MIA", 500)
    graph.add_edge("HNL", "SFO", 130)
    graph.add_edge("HNL", "LAX", 250)
    graph.add_edge("SFO", "HNL", 130)
    graph.add_edge("SFO", "LAX", 60)
    graph.add_edge("SFO", "ORD", 70)
    graph.add_edge("LAX", "HNL", 250)
    graph.add_edge("LAX", "SFO", 60)
    graph.add_edge("LAX", "ORD", 170)
    graph.add_edge("LAX", "DFW", 120)
    graph.add_edge("ORD", "SFO", 70)
    graph.add_edge("ORD", "LAX", 170)
    graph.add_edge("ORD", "DFW", 80)
    graph.add_edge("ORD", "PVD", 50)
    graph.add_edge("DFW", "LAX", 120)
    graph.add_edge("DFW", "ORD", 80)
    graph.add_edge("DFW", "LGA", 140)
    graph.add_edge("DFW", "MIA", 110)
    graph.add_edge("PVD", "ORD", 50)
    graph.add_edge("PVD", "LGA", 200)
    graph.add_edge("LGA", "DFW", 140)
    graph.add_edge("LGA", "MIA", 100)
    graph.add_edge("MIA", "DFW", 110)
    graph.add_edge("MIA", "HNL", 500)

    # inspect cheapest route from San Francisco to Miami A*
    print("A* Cheapest Route (SFO->MIA)")
    for node in graph.astar("SFO", "MIA"):
        if node.prev:
            print(f'{node.prev}->{node} $({node.cost - node.prev.cost})')
    print()

    # inspect cheapest route from San Francisco to Miami Dijkstra
    print("Dijkstra Cheapest Route (SFO->MIA)")
    for node in graph.dijkstra("SFO", "MIA"):
        if node.prev:
            print(f'{node.prev}->{node} $({node.cost - node.prev.cost})')
    print()

    # inspect fastest route from San Francisco to Miami, irrespective of cost
    print("BFS Fastest Route (SFO->MIA)")
    for node in graph.bfs("SFO", "MIA"):
        if node.prev:
            print(f'{node.prev}->{node} $({node.cost - node.prev.cost})')
    print()

    # inspect dfs route from San Francisco to Miami
    print("DFS Route (SFO->MIA)")
    for node in graph.dfs("SFO", "MIA"):
        if node.prev:
            print(f'{node.prev}->{node} $({node.cost - node.prev.cost})')

    toc = time.time()
    print(f'Took: {toc-tic} seconds.')

if __name__ == '__main__':
    main()
